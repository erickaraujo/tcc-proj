import logging

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)

counter = 0
logging.debug("counter value is {0}".format(counter))

counter = 7
mod = 7
logging.debug("What's result: {0} % {1}...".format(counter, mod))
logging.debug(counter%mod)