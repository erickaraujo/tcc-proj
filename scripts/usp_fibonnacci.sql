DELIMITER $$

DROP PROCEDURE IF EXISTS usp_fibonacci_iterativo$$
CREATE PROCEDURE usp_test(IN v_number int)
	BEGIN
		DECLARE v_result, v_int1, v_int2, v_int3 INT; 
        SET v_int1 = 0;
        SET v_int2 = 1;
        
        WHILE v_int2 <= v_number DO
			BEGIN
				SET v_int3 = v_int1 + v_int2;
				SET v_int1 = v_int2;
				SET v_int2 = v_int3;
				IF MOD(v_int2, 2) = 0 THEN SET v_result =+ v_int2; END IF;
            END;
		END WHILE;
    SELECT v_result as 'Total';
    END$$
    
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS usp_fibonacci_recursivo$$
CREATE PROCEDURE usp_test2(IN v_handler INT)
	BEGIN
        
		WHILE !(v_handler <= 1) DO
			SELECT v_handler;
			SET v_handler = (v_handler - 1) + (v_handler - 2);
		END WHILE;
        
        SELECT v_handler;
    END$$
    
DELIMITER ;