-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--


DROP TABLE IF EXISTS _rdebug_breakpoint_hints;

CREATE TABLE _rdebug_breakpoint_hints (
  worker_id bigint unsigned not null, 
  routine_schema varchar(64) not null, 
  routine_name varchar(64) not null, 
  statement_id int unsigned not null,
  conditional_expression text default null,
  PRIMARY KEY (worker_id, routine_schema, routine_name, statement_id)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _rdebug_routine_statements;

CREATE TABLE _rdebug_routine_statements (
  routine_schema varchar(64) not null, 
  routine_name varchar(64) not null, 
  statement_id int unsigned not null not null,
  statement_start_pos int unsigned default 0, 
  statement_end_pos int unsigned default 0,
  PRIMARY KEY (routine_schema, routine_name, statement_id)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _rdebug_routine_variables;

CREATE TABLE _rdebug_routine_variables (
  routine_schema varchar(64) not null, 
  routine_name varchar(64) not null, 
  variable_name varchar(128) not null,
  variable_scope_id_start int unsigned not null default 0, 
  variable_scope_id_end int unsigned default 0,
  variable_type enum('param', 'local', 'user_defined') not null,
  PRIMARY KEY (routine_schema, routine_name, variable_name, variable_scope_id_start)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _rdebug_routine_variables_state;

CREATE TABLE _rdebug_routine_variables_state (
  worker_id bigint unsigned not null, 
  stack_level int unsigned not null,
  routine_schema varchar(64) not null, 
  routine_name varchar(64) not null, 
  variable_name varchar(128) not null,
  variable_value blob default null,
  modify_time timestamp,
  PRIMARY KEY (worker_id, stack_level, routine_schema, routine_name, variable_name)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _rdebug_stack_state;

CREATE TABLE _rdebug_stack_state (
  worker_id bigint unsigned not null, 
  stack_level int unsigned not null,
  routine_schema varchar(64) not null, 
  routine_name varchar(64) not null, 
  statement_id int unsigned not null,
  entry_time timestamp default CURRENT_TIMESTAMP,
  PRIMARY KEY (worker_id, stack_level)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _rdebug_stats;

CREATE TABLE _rdebug_stats (
  worker_id bigint unsigned not null, 
  routine_schema varchar(64) not null, 
  routine_name varchar(64) not null, 
  statement_id int unsigned not null,
  count_visits bigint unsigned not null,
  PRIMARY KEY (worker_id, routine_schema, routine_name, statement_id)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _rdebug_step_hints;

CREATE TABLE _rdebug_step_hints (
  worker_id bigint unsigned not null, 
  hint_type enum ('step_into', 'step_over', 'step_out', 'run') not null default 'step_into',
  stack_level int unsigned default null,
  is_consumed tinyint unsigned default 0,
  PRIMARY KEY (worker_id)
) ENGINE=MyISAM ;

DROP TABLE IF EXISTS _script_statements;

CREATE TABLE _script_statements (
  statement varchar(16) CHARACTER SET ascii NOT NULL,
  statement_type enum('sql', 'script', 'script,sql', 'unknown') DEFAULT NULL,
  PRIMARY KEY (statement)
) ENGINE=InnoDB ;

--
-- SQL statements
--
INSERT INTO _script_statements VALUES ('alter', 'sql');
INSERT INTO _script_statements VALUES ('analyze', 'sql');
INSERT INTO _script_statements VALUES ('binlog', 'sql');
INSERT INTO _script_statements VALUES ('cache', 'sql');
INSERT INTO _script_statements VALUES ('call', 'sql');
INSERT INTO _script_statements VALUES ('change', 'sql');
INSERT INTO _script_statements VALUES ('check', 'sql');
INSERT INTO _script_statements VALUES ('checksum', 'sql');
INSERT INTO _script_statements VALUES ('create ', 'sql');
INSERT INTO _script_statements VALUES ('delete', 'sql');
INSERT INTO _script_statements VALUES ('desc', 'sql');
INSERT INTO _script_statements VALUES ('describe', 'sql');
INSERT INTO _script_statements VALUES ('do', 'sql');
INSERT INTO _script_statements VALUES ('drop', 'sql');
INSERT INTO _script_statements VALUES ('drop user', 'sql');
INSERT INTO _script_statements VALUES ('flush', 'sql');
INSERT INTO _script_statements VALUES ('grant', 'sql');
INSERT INTO _script_statements VALUES ('handler', 'sql');
INSERT INTO _script_statements VALUES ('insert', 'sql');
INSERT INTO _script_statements VALUES ('kill', 'sql');
INSERT INTO _script_statements VALUES ('load', 'sql');
INSERT INTO _script_statements VALUES ('lock', 'sql');
INSERT INTO _script_statements VALUES ('optimize', 'sql');
INSERT INTO _script_statements VALUES ('purge', 'sql');
INSERT INTO _script_statements VALUES ('rename', 'sql');
INSERT INTO _script_statements VALUES ('repair', 'sql');
INSERT INTO _script_statements VALUES ('replace', 'sql');
INSERT INTO _script_statements VALUES ('reset', 'sql');
INSERT INTO _script_statements VALUES ('revoke', 'sql');
INSERT INTO _script_statements VALUES ('savepoint', 'sql');
INSERT INTO _script_statements VALUES ('select', 'sql');
INSERT INTO _script_statements VALUES ('set', 'sql');
INSERT INTO _script_statements VALUES ('show', 'sql');
INSERT INTO _script_statements VALUES ('stop ', 'sql');
INSERT INTO _script_statements VALUES ('truncate', 'sql');
INSERT INTO _script_statements VALUES ('unlock', 'sql');
INSERT INTO _script_statements VALUES ('update', 'sql');

--
-- Script statements
--
INSERT INTO _script_statements VALUES ('echo', 'script');
INSERT INTO _script_statements VALUES ('eval', 'script');
INSERT INTO _script_statements VALUES ('invoke', 'script');
INSERT INTO _script_statements VALUES ('pass', 'script');
INSERT INTO _script_statements VALUES ('sleep', 'script');
INSERT INTO _script_statements VALUES ('throttle', 'script');
INSERT INTO _script_statements VALUES ('throw', 'script');
INSERT INTO _script_statements VALUES ('var', 'script');
INSERT INTO _script_statements VALUES ('input', 'script');
INSERT INTO _script_statements VALUES ('report', 'script');
INSERT INTO _script_statements VALUES ('begin', 'script');
INSERT INTO _script_statements VALUES ('commit', 'script');
INSERT INTO _script_statements VALUES ('rollback', 'script');

--
-- Both SQL and Script statements (ambiguous resolve)
--
INSERT INTO _script_statements VALUES ('start', 'script,sql');

DROP TABLE IF EXISTS _waits;

CREATE TABLE _waits (
  wait_name varchar(128) character set ascii collate ascii_bin NOT NULL,
  wait_value bigint unsigned not null,
  first_entry_time timestamp default CURRENT_TIMESTAMP,
  last_entry_time timestamp null default null,
  PRIMARY KEY (wait_name),
  KEY (last_entry_time)
) ENGINE=InnoDB ;

---- 
-- Metadata: information about this project
-- 
DROP TABLE IF EXISTS help_content;

CREATE TABLE help_content (
  topic VARCHAR(32) CHARSET ascii NOT NULL,
  help_message TEXT CHARSET utf8 NOT NULL,
  PRIMARY KEY (topic)
) ENGINE=InnoDB
;
-- 
-- Metadata: information about this project
-- 
DROP TABLE IF EXISTS metadata;

CREATE TABLE metadata (
  `attribute_name` VARCHAR(64) CHARSET ascii NOT NULL,
  `attribute_value` VARCHAR(2048) CHARSET utf8 NOT NULL,
  PRIMARY KEY (`attribute_name`)
) ENGINE=InnoDB
;

--
-- 
--
INSERT INTO metadata (attribute_name, attribute_value) VALUES
  ('author', 'Shlomi Noach'),
  ('author_url', 'http://code.openark.org/blog/shlomi-noach'),
  ('install_success', false),
  ('install_time', NOW()),
  ('install_sql_mode', @@sql_mode),
  ('install_mysql_version', VERSION()),
  ('base_components_installed', false),
  ('innodb_plugin_components_installed', false),
  ('percona_server_components_installed', false),
  ('tokudb_components_installed', false),
  ('license_type', 'GPL'),
  ('license', '

common_schema - DBA''s Framework for MySQL
Copyright (C) 2011-2013, Shlomi Noach

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
<http://www.gnu.org/licenses/>; or type ''man gpl'' on a unix system.
'),
  ('project_name', 'common_schema'),
  ('project_home', 'http://code.google.com/p/common-schema/'),
  ('project_repository', 'https://common-schema.googlecode.com/svn/trunk/'),
  ('project_repository_type', 'svn'),
  ('revision', '0'),
  ('version', '2.2')
;  


DELIMITER $$

DROP procedure IF EXISTS _rdebug_analyze_routine $$
CREATE procedure _rdebug_analyze_routine(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8
)
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''

main_body: BEGIN
  declare current_id int unsigned;
  declare current_pos int unsigned;
  declare current_token text charset utf8;
  declare current_state varchar(32);
  declare is_beginning_of_statement tinyint unsigned default false;
  declare is_labeled_statement tinyint unsigned default false;
  declare expected_token varchar(32) charset ascii default null;
  declare next_nesting_block_id int unsigned default 0;
  declare declare_variables_statement_id int unsigned default null;
  declare valid_breakpoint_statement_id int unsigned default null;
  declare valid_breakpoint_statement_pos int unsigned default null;
  declare first_begin_id int unsigned default null;
  declare first_statement_id int unsigned default null;
  
  declare cursor_done tinyint unsigned default false;
  declare tokens_cursor cursor for select id, start, token, state from _routine_tokens;
  declare continue handler for not found set cursor_done := true;

  update _routine_tokens set nesting_level = 0, is_valid_for_breakpoint = 0, is_declare_variables_statement = 0, is_first_statement = 0;
  delete from _rdebug_routine_statements where routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name;
  delete from _rdebug_routine_variables where routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name;
  delete from _rdebug_routine_variables_state where routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name;
  delete from _rdebug_breakpoint_hints where routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name;

  open tokens_cursor;
  tokens_cursor_loop: loop
    fetch tokens_cursor into current_id, current_pos, current_token, current_state; 
    if cursor_done then
      leave tokens_cursor_loop;
    end if;
    set current_token := lower(current_token);
    if current_state in ('whitespace', 'single line comment', 'multi line comment') then
      iterate tokens_cursor_loop;
    end if;
    if current_state in ('colon') and first_begin_id is not null then
      -- This is a label. However, we completely ignore the label for the BEGIN statement 
      -- of the routine (hecne check for first_begin_id), since it has nothing to do with statements...
      set is_beginning_of_statement := true;
      set is_labeled_statement := true;
      set expected_token := '';
      iterate tokens_cursor_loop;
    end if;
    if (current_state, current_token) = ('alpha', 'begin') then
      if first_begin_id is null then
        set first_begin_id := current_id;
      end if;
      update _routine_tokens set nesting_level = nesting_level + 1 where id >= current_id;
      set next_nesting_block_id := next_nesting_block_id + 1;
    end if;
    
    if current_state = 'user-defined variable' then
      insert ignore into 
        _rdebug_routine_variables (routine_schema, routine_name, variable_name, variable_scope_id_start, variable_scope_id_end, variable_type)
        values (rdebug_routine_schema, rdebug_routine_name, current_token, 0, NULL, 'user_defined');
    end if;
    
    if (current_state, current_token) = ('alpha', 'begin') or (current_state in ('statement delimiter', 'start')) then
      set is_beginning_of_statement := true;
      if declare_variables_statement_id is not null then
        -- We are now terminating a "declare [var]" statement.
        update _routine_tokens set is_declare_variables_statement = 1 where id = declare_variables_statement_id;
        -- This is a good place to analyze variable names!
        call _rdebug_analyze_routine_declare_statement(rdebug_routine_schema, rdebug_routine_name, declare_variables_statement_id, current_id);
        set declare_variables_statement_id := null;
      end if;
      if valid_breakpoint_statement_id is not null then
        -- this is end of statement. Note!
        insert into 
          _rdebug_routine_statements (routine_schema, routine_name, statement_id, statement_start_pos, statement_end_pos)
        values
          (rdebug_routine_schema, rdebug_routine_name, valid_breakpoint_statement_id, valid_breakpoint_statement_pos, current_pos);
        set valid_breakpoint_statement_id := null;
      end if;
      iterate tokens_cursor_loop;
    end if;
    if expected_token = '' then
      set expected_token := null;
    end if;
    if expected_token is not null and expected_token != current_token then
      iterate tokens_cursor_loop;
    end if;
    if expected_token = current_token then
      -- Found the "expected". But we do not consume it -- we skip it.
      set expected_token := null;

      -- And for some statements (like IF-THEN)
      if valid_breakpoint_statement_id is not null then
        -- this is end of statement. Note!
        insert into 
          _rdebug_routine_statements (routine_schema, routine_name, statement_id, statement_start_pos, statement_end_pos)
        values
          (rdebug_routine_schema, rdebug_routine_name, valid_breakpoint_statement_id, valid_breakpoint_statement_pos, current_pos);
        set valid_breakpoint_statement_id := null;
      end if;
      
      iterate tokens_cursor_loop;
    end if;
    if declare_variables_statement_id is not null and current_token in ('cursor', 'handler', 'condition') then
      -- Not a variable declaration
      set declare_variables_statement_id := null;
    end if;
    
    -- Beginning of a statement:
    if is_beginning_of_statement then
      if first_statement_id is null then
        update _routine_tokens set is_first_statement = 1 where id = current_id;    
        set first_statement_id := current_id;
      end if;
      case current_token 
        when 'if' then set expected_token := 'then';
        when 'else' then set expected_token := '';
        when 'while' then set expected_token := 'do';
        when 'loop' then set expected_token := '';
        when 'repeat' then set expected_token := '';
        when 'when' then set expected_token := 'then';
        else begin end;
      end case;
      if current_token in ('if', 'while', 'repeat', 'loop', 'case') then
        update _routine_tokens set nesting_level = nesting_level + 1 where id >= current_id;
        set next_nesting_block_id := next_nesting_block_id + 1;
      end if;
      if current_token = 'end' then
        update _routine_tokens set nesting_level = nesting_level - 1 where id > current_id;
      end if;
      if (current_token = 'declare') then
        set declare_variables_statement_id := current_id;
      elseif (expected_token is null) or (current_token in ('if', 'while', 'loop', 'repeat')) then
        if not is_labeled_statement then
          -- a labeled statement is one such that is preceeded by a label.
          -- The label position makes for the actual "valid for breakpoint"
          -- but the statement itself is of importance as we ened to figure out how
          -- to handle it (if it's a "WHILE", look for "DO")
          update _routine_tokens set is_valid_for_breakpoint = 1 where id = current_id;
          set valid_breakpoint_statement_id := current_id;
          set valid_breakpoint_statement_pos := current_pos;
        end if;
      end if;
      set is_labeled_statement := false;
      if expected_token is not null then
        iterate tokens_cursor_loop;
      end if;
      set is_beginning_of_statement := false;
    end if;
  end loop;
  close tokens_cursor;
END $$

DELIMITER ;

-- 
-- Extract variable names from a 'declare' statement.
-- The statement is assumed to declare variables (ass opposed to declare cursor, handler, state).
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_analyze_routine_declare_statement $$
CREATE procedure _rdebug_analyze_routine_declare_statement(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8,
  in declare_id int unsigned,
  in declare_end_id int unsigned
)
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''

main_body: BEGIN
  declare current_id int unsigned;
  declare current_token text charset utf8;
  declare current_state varchar(32);
  declare variable_expected tinyint unsigned default true;
  
  declare cursor_done tinyint unsigned default false;
  declare variables_cursor cursor for select id, token, state from _routine_tokens where id between declare_id + 1 and declare_end_id;
  declare continue handler for not found set cursor_done := true;

  open variables_cursor;
  cursor_loop: loop
    fetch variables_cursor into current_id, current_token, current_state; 
    if cursor_done then
      leave cursor_loop;
    end if;
    if current_state = 'whitespace' then
      iterate cursor_loop;
    end if;
    if current_state = 'comma' then
      set variable_expected := true;
      iterate cursor_loop;
    end if;
    if not variable_expected then
      leave cursor_loop;
    end if;
    
    insert ignore into 
      _rdebug_routine_variables (routine_schema, routine_name, variable_name, variable_scope_id_start, variable_scope_id_end, variable_type)
      values (rdebug_routine_schema, rdebug_routine_name, current_token, declare_id, NULL, 'local');
    set variable_expected := false;
  end loop;
  close variables_cursor;
END $$

DELIMITER ;

-- 
-- Analyze the parameters for a given routine; write down their names
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_analyze_routine_params $$
CREATE procedure _rdebug_analyze_routine_params(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8,
  in variables_routine_param_list blob,
  in quoting_characters VARCHAR(5)
)
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''

main_body: BEGIN
  begin
    -- param list
    declare param_index int unsigned default 1;
    declare param_text text charset utf8;
  
    set variables_routine_param_list := replace_sections(variables_routine_param_list, '(', ')', '');
    set variables_routine_param_list := common_schema._retokenized_text(variables_routine_param_list, ',', quoting_characters, true, 'skip');
  
    while param_index <= ifnull(@common_schema_retokenized_count, 0) do
      set param_text := split_token(variables_routine_param_list, @common_schema_retokenized_delimiter, param_index);
      set param_text := replace_all(param_text, ' \n\r\b\t', ' ');
      if (locate('in ', lower(param_text)) = 1) or (locate('out ', lower(param_text)) = 1) or (locate('inout ', lower(param_text)) = 1) then
        set param_text = trim(substring(param_text, locate(' ', param_text)));
      end if;
      set param_text = trim(left(param_text, locate(' ', param_text)));
      insert into 
        _rdebug_routine_variables (routine_schema, routine_name, variable_name, variable_scope_id_start, variable_scope_id_end, variable_type)
        values (rdebug_routine_schema, rdebug_routine_name, param_text, 0, NULL, 'param');
      set param_index := param_index + 1;
    end while;
  end;
END $$

DELIMITER ;

-- 
-- Per variable, detect its scope end
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_analyze_routine_variables_scope $$
CREATE procedure _rdebug_analyze_routine_variables_scope(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8
)
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''

main_body: BEGIN
  declare current_variable_name varchar(128);
  declare current_variable_scope_id_start int unsigned;
  declare current_nesting_level int unsigned;
  declare scope_end_id int unsigned;
  
  declare cursor_done tinyint unsigned default false;
  declare variables_cursor cursor for 
    select variable_name, variable_scope_id_start, nesting_level
    from 
      _rdebug_routine_variables
      join _routine_tokens on (variable_scope_id_start = _routine_tokens.id)
    where routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name and variable_type = 'local';
  declare continue handler for not found set cursor_done := true;

  open variables_cursor;
  cursor_loop: loop
    fetch variables_cursor into current_variable_name, current_variable_scope_id_start, current_nesting_level; 
    if cursor_done then
      leave cursor_loop;
    end if;
    -- Find end of block for current variable (by finding first time nesting level is smaller than current)
    select min(id) - 1 
      from _routine_tokens
      where id >= current_variable_scope_id_start and nesting_level < current_nesting_level
      into scope_end_id;

    update _rdebug_routine_variables set variable_scope_id_end = scope_end_id 
      where 
        routine_schema = rdebug_routine_schema 
        and routine_name = rdebug_routine_name 
        and variable_name = current_variable_name 
        and variable_scope_id_start = current_variable_scope_id_start;
  end loop;
  close variables_cursor;
  
  select max(id) from _routine_tokens into scope_end_id;
  update _rdebug_routine_variables set variable_scope_id_end = scope_end_id 
    where routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name and variable_type in ('user_defined', 'param');
END $$

DELIMITER ;
-- 
-- Write down current variable (variables within current scope)
-- into global variables state table
-- 


DELIMITER $$

DROP procedure IF EXISTS _rdebug_evaluate_condition $$
CREATE procedure _rdebug_evaluate_condition(
    in  rdebug_condition text,
    out rdebug_condition_result bool
  )
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: begin
  if rdebug_condition is null or rdebug_condition = '' then
    set rdebug_condition_result := true;
    leave main_body;
  end if;
  select 
      concat('
        select (', rdebug_condition, ') is true into @_common_schema_rdebug_condition_result'
      )
    into @_rdebug_command
  ;

  prepare st from @_rdebug_command;
  execute st;
  deallocate prepare st;

  set rdebug_condition_result := @_common_schema_rdebug_condition_result;
END $$

DELIMITER ;
-- 
-- Write down current variables (variables within current scope)
-- into global variables state table
-- 
DELIMITER $$

DROP procedure IF EXISTS _rdebug_export_variable_state $$
CREATE procedure _rdebug_export_variable_state(
    in breakpoint_id int unsigned,
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

BEGIN
  select 
      concat('
        insert into _rdebug_routine_variables_state
          (worker_id, stack_level, routine_schema, routine_name, variable_name, variable_value)
        values
        ', 
          group_concat(
            '(CONNECTION_ID(), @_rdebug_stack_level_, ', QUOTE(rdebug_routine_schema), ',', QUOTE(rdebug_routine_name),
            ',', QUOTE(variable_name), ',', IF(variable_type IN ('param', 'local'), CONCAT('@$_$', variable_name), variable_name), ')'
           ),
        ' on duplicate key update variable_value=VALUES(variable_value)'
      )
    from
      _rdebug_routine_variables 
    where
      breakpoint_id between _rdebug_routine_variables.variable_scope_id_start and _rdebug_routine_variables.variable_scope_id_end
      and routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name
    into @_rdebug_command
  ;
  if @_rdebug_command is not null then
    -- Could be NULL when _rdebug_routine_variables has nothing for us
    prepare st from @_rdebug_command;
    execute st;
    deallocate prepare st;
  end if;
END $$

DELIMITER ;


DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_debug_code_breakpoint_hint $$
CREATE FUNCTION _rdebug_get_debug_code_breakpoint_hint() returns varchar(64) 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT ''

begin
  return '/*[B]*/';
end $$

DELIMITER ;
--
-- 
-- 
-- 
DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_debug_code_breakpoint_hint_end $$
CREATE FUNCTION _rdebug_get_debug_code_breakpoint_hint_end() returns varchar(64) 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT ''

begin
  return ':]*/';
end $$

DELIMITER ;

-- 
-- 
-- 

DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_debug_code_breakpoint_hint_start $$
CREATE FUNCTION _rdebug_get_debug_code_breakpoint_hint_start() returns varchar(64) 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT ''

begin
  return '/*[B:';
end $$

DELIMITER ;

-- 
-- 
-- 

DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_debug_code_end $$
CREATE FUNCTION _rdebug_get_debug_code_end() returns varchar(64) 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT ''

begin
  return '/* [/_common_schema_debug_] */';
end $$

DELIMITER ;

-- 
-- 
-- 

DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_debug_code_start $$
CREATE FUNCTION _rdebug_get_debug_code_start() returns varchar(64) 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT ''

begin
  -- The reson the next text is broken is that we wish to avoid mistakenly identify this
  -- very routines as being a "with debug mode" due to the appearance of the magic
  -- start-code. Much like "ps aux | grep ... | grep -v grep"
  return CONCAT('/* [_common_schema_debug_', '] */');
end $$

DELIMITER ;

--
--
--

DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_lock_name $$
CREATE FUNCTION _rdebug_get_lock_name(
  connection_id int unsigned,
  name_hint varchar(32)
  ) returns varchar(64) 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT 'Return value of option in JS options format'

begin
  return CONCAT_WS('_', '__common_schema_rdebug_lock', connection_id, name_hint);
end $$

DELIMITER ;

-- 
-- Executed by the worker
-- 

DELIMITER $$

DROP FUNCTION IF EXISTS _rdebug_get_next_stack_level $$
CREATE FUNCTION _rdebug_get_next_stack_level() returns int unsigned 
DETERMINISTIC
READS SQL DATA
SQL SECURITY DEFINER
COMMENT 'Return next in order stack level'

begin
  declare next_stack_level int unsigned default null;
  
  select 
      max(stack_level) 
    from 
      _rdebug_stack_state
    where
      worker_id = CONNECTION_ID()
    into next_stack_level;
  
  if next_stack_level is null
    then return 1;
  end if;
  return IFNULL(@_rdebug_stack_level_ + 1, 1);
end $$

DELIMITER ;

-- 
-- Import variables within current scope
-- from global variables state table
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_import_variable_state $$
CREATE procedure _rdebug_import_variable_state(
    in breakpoint_id int unsigned,
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

BEGIN
  select 
      concat('
        select ',
          group_concat('max(if(variable_name=', QUOTE(variable_name), ', variable_value, NULL))' order by variable_name), '
        from
          _rdebug_routine_variables_state
        where
          worker_id = CONNECTION_ID()
          and stack_level = @_rdebug_stack_level_
          and routine_schema = ', QUOTE(rdebug_routine_schema), ' and routine_name = ', QUOTE(rdebug_routine_name), '
        into ',
          group_concat(IF(variable_type IN ('param', 'local'), CONCAT('@$_$', variable_name), variable_name) order by variable_name)
      )
    from
      _rdebug_routine_variables 
    where
      breakpoint_id between _rdebug_routine_variables.variable_scope_id_start and _rdebug_routine_variables.variable_scope_id_end
      and routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name
    into @_rdebug_command
  ;
  -- select @_rdebug_command;
  if @_rdebug_command is not null then
    -- Could be NULL when _rdebug_routine_variables has nothing for us
    prepare st from @_rdebug_command;
    execute st;
    deallocate prepare st;
  end if;
END $$

DELIMITER ;

-- 
-- 
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_invalidate_routine_cache $$
CREATE procedure _rdebug_invalidate_routine_cache()
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY DEFINER

BEGIN
  create or replace view _rdebug_invalidate_routine_cache_view as select 1 as `invalidated`;
END $$

DELIMITER ;

-- 
-- Called upon breakpoint code
-- Does not necessarily mean the breakpoint should be active.
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_on_breakpoint 
$$
CREATE procedure _rdebug_on_breakpoint(
    in breakpoint_id int unsigned,
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  declare is_active_breakpoint bool default true;
  declare is_dedicated_breakpoint bool default true;
  declare rdebug_conditional_expression text default null;
  declare have_waited bool default false;

  select count(*) > 0 from _rdebug_step_hints 
    where worker_id = connection_id() 
    and (
      (hint_type = 'step_into')
      or (hint_type = 'step_over' and @_rdebug_stack_level_ <= stack_level)
      or (hint_type = 'step_out' and @_rdebug_stack_level_ < stack_level)
    )
    into is_active_breakpoint
  ;

  if not is_active_breakpoint then
    -- another chance: is there a dedicated breakpoint here?
    select count(statement_id) > 0, min(conditional_expression)
      from _rdebug_breakpoint_hints
      where 
        worker_id = connection_id()
        and routine_schema = rdebug_routine_schema
        and routine_name = rdebug_routine_name
        and statement_id = breakpoint_id
      into is_dedicated_breakpoint, rdebug_conditional_expression;
    if is_dedicated_breakpoint and rdebug_conditional_expression is null then
      -- explicit, non-conditional breakpoint here
      set is_active_breakpoint := true;
    end if;
  end if;

  if not is_active_breakpoint then
    leave main_body;
  end if;

  if is_used_lock(_rdebug_get_lock_name(connection_id(), 'tic')) then
    while is_used_lock(_rdebug_get_lock_name(connection_id(), 'tic')) != connection_id() do
      if not have_waited then
        call _rdebug_on_breakpoint_start_wait(breakpoint_id, rdebug_routine_schema, rdebug_routine_name);
      end if;
      do sleep(0.1 + coalesce(0, 'rdebug_worker_waiting_on_breakpoint'));
      set have_waited := true;
    end while;
  else
    while is_used_lock(_rdebug_get_lock_name(connection_id(), 'toc')) != connection_id() do
      if not have_waited then
        call _rdebug_on_breakpoint_start_wait(breakpoint_id, rdebug_routine_schema, rdebug_routine_name);
      end if;
      do sleep(0.1 + coalesce(0, 'rdebug_worker_waiting_on_breakpoint'));
      set have_waited := true;
    end while;
  end if;

  if have_waited then
    call _rdebug_on_breakpoint_end_wait(breakpoint_id, rdebug_routine_schema, rdebug_routine_name);
  end if;
END $$

DELIMITER ;

-- 
-- Called upon an active breakpoint, just as the worker starts waiting.
-- This method is only called once per breakpoint entry. 
--
 
DELIMITER $$

DROP procedure IF EXISTS _rdebug_on_breakpoint_end_wait $$
CREATE procedure _rdebug_on_breakpoint_end_wait(
    in breakpoint_id int unsigned,
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call _rdebug_import_variable_state(breakpoint_id, rdebug_routine_schema, rdebug_routine_name);
  -- do release_lock(_rdebug_get_lock_name(connection_id(), 'breakpoint'));
END $$

DELIMITER ;

-- 
-- Called upon an active breakpoint, just as the worker starts waiting.
-- This method is only called once per breakpoint entry. 
--
 
DELIMITER $$

DROP procedure IF EXISTS _rdebug_on_breakpoint_start_wait $$
CREATE procedure _rdebug_on_breakpoint_start_wait(
    in breakpoint_id int unsigned,
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  delete from _rdebug_stack_state 
    where worker_id = connection_id() and stack_level > @_rdebug_stack_level_;
  delete from _rdebug_routine_variables_state 
    where worker_id = connection_id() and stack_level > @_rdebug_stack_level_;
    
  insert into _rdebug_stack_state 
      (worker_id, stack_level, routine_schema, routine_name, statement_id) 
    values 
      (connection_id(), @_rdebug_stack_level_, rdebug_routine_schema, rdebug_routine_name, breakpoint_id)
    on duplicate key update 
      routine_schema = VALUES(routine_schema),
      routine_name = VALUES(routine_name),
      statement_id = VALUES(statement_id);
  
  call _rdebug_export_variable_state(breakpoint_id, rdebug_routine_schema, rdebug_routine_name);

  call _rdebug_send_breakpoint_clock();
END $$

DELIMITER ;

-- 
-- Called upon breakpoint code
-- Does not necessarily mean the breakpoint should be active.
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_on_routine_entry 
$$
CREATE procedure _rdebug_on_routine_entry(
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN	
  if is_used_lock(_rdebug_get_lock_name(connection_id(), 'worker')) is null then
    leave main_body;
  end if;

  insert into _rdebug_stats (
    worker_id, routine_schema, routine_name, statement_id, count_visits)
    values (connection_id(), rdebug_routine_schema, rdebug_routine_name, 0, 1)
    on duplicate key update count_visits = count_visits + 1;
END $$

DELIMITER ;

--
-- 
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_release_worker_and_wait_for_breakpoint $$
CREATE procedure _rdebug_release_worker_and_wait_for_breakpoint()
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

BEGIN
  call thread_wait(
    concat('breakpoint_clock_', @_rdebug_recipient_id), 
    0.1, 
    'call _rdebug_send_clock()', 
    concat('
       (select ifnull(max(command)=''Sleep'', true) from information_schema.processlist where id=', @_rdebug_recipient_id, ')
       and (select count(*) > 0 from _rdebug_stack_state where worker_id=',@_rdebug_recipient_id, ')
    ')
  );
  select ifnull(max(command)='Sleep', true) from information_schema.processlist where id=@_rdebug_recipient_id into @_rdebug_worker_done;
  if @_rdebug_worker_done then
    -- Identify the case where the worker has actually quit (last routine exited)
    delete from _rdebug_stack_state where worker_id = @_rdebug_recipient_id;
  end if;
  if @_rdebug_verbose and not @_rdebug_worker_done then
    call rdebug_verbose();
  end if;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Sent by the worker when entring a breakpoint wait 
--

DELIMITER $$

DROP procedure IF EXISTS _rdebug_send_breakpoint_clock $$
CREATE procedure _rdebug_send_breakpoint_clock()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call thread_notify(concat('breakpoint_clock_', connection_id()));
  leave main_body;
  
  select 
    if(
      is_used_lock(_rdebug_get_lock_name(connection_id(), 'breakpoint_tic')) = connection_id(),
      release_lock(_rdebug_get_lock_name(connection_id(), 'breakpoint_tic')) + get_lock(_rdebug_get_lock_name(connection_id(), 'breakpoint_toc'), 100000000),
      release_lock(_rdebug_get_lock_name(connection_id(), 'breakpoint_toc')) + get_lock(_rdebug_get_lock_name(connection_id(), 'breakpoint_tic'), 100000000)
    )
  into @common_schema_dummy;
END $$

DELIMITER ;

-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- 

DELIMITER $$

DROP procedure IF EXISTS _rdebug_send_clock $$
CREATE procedure _rdebug_send_clock()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

BEGIN
  select 
    if(
      is_used_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'tic')) = connection_id(),
      release_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'tic')) + get_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'toc'), 100000000),
      release_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'toc')) + get_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'tic'), 100000000)
    )
  into @common_schema_dummy;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Step into next breakpoint in current stack level or above (does not drill in).
--

DELIMITER $$

DROP procedure IF EXISTS _rdebug_set_step_hint $$
CREATE procedure _rdebug_set_step_hint(
  rdebug_hint_type varchar(32)
)
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  declare rdebug_stack_level int unsigned default null;
  
  select
      ifnull(max(stack_level), 1)
    from _rdebug_stack_state
    where worker_id = @_rdebug_recipient_id
    group by worker_id
    into rdebug_stack_level;

  insert into 
      _rdebug_step_hints (worker_id, hint_type, stack_level, is_consumed)
    values 
      (@_rdebug_recipient_id, rdebug_hint_type, ifnull(rdebug_stack_level,1), 0)
    on duplicate key update 
      hint_type = values(hint_type), stack_level = values(stack_level), is_consumed = values(is_consumed) 
    ;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
---- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_compile_routine $$
CREATE procedure rdebug_compile_routine(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8,
  in debug_info     bool
  )
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''

BEGIN
  declare quoting_characters VARCHAR(5) CHARSET ascii DEFAULT '`''';
  declare compiled_body, compiled_body_utf8 longblob;
  declare debug_code text charset utf8 default '';
  declare first_statement_debug_code text charset utf8;
  declare first_valid_for_breakpoint_debug_code text charset utf8;
  declare first_valid_for_breakpoint_id int unsigned default null;
  declare debug_code_start varchar(64) charset utf8 default _rdebug_get_debug_code_start();
  declare debug_code_end varchar(64) charset utf8 default _rdebug_get_debug_code_end();
  declare routine_param_list blob default null;

  set @__debug_group_concat_max_len := @@group_concat_max_len;
  set @@group_concat_max_len := GREATEST(@@group_concat_max_len, 32 * 1024 * 1024);
  
  if not find_in_set('ANSI_QUOTES', @@sql_mode) then
    set quoting_characters := CONCAT(quoting_characters, '"');
  end if;

  -- Get current routine body:
  set compiled_body := null;
  select body, body_utf8, param_list from mysql.proc where db = rdebug_routine_schema and name = rdebug_routine_name into compiled_body, compiled_body_utf8, routine_param_list;
  if compiled_body is null then
    call throw(CONCAT('Unknown routine: ', mysql_qualify(rdebug_routine_schema), '.', mysql_qualify(rdebug_routine_name)));
  end if;
  
  -- Remove any existing debug code
  set compiled_body := replace_sections(compiled_body, debug_code_start, debug_code_end, '');
  set compiled_body_utf8 := replace_sections(compiled_body_utf8, debug_code_start, debug_code_end, '');

  begin
    declare v_from, v_old_from int unsigned;
    declare v_token text;
    declare v_level int;
    declare v_state varchar(32);
    declare _sql_tokens_id int unsigned default 0;
    
    drop temporary table if exists _routine_tokens;
    create temporary table _routine_tokens(
        id int unsigned primary key
    ,   start int unsigned  not null
    ,   level int not null
    ,   token text          
    ,   state text           not null
    ,   nesting_level int unsigned default 0
    ,   is_valid_for_breakpoint tinyint unsigned default 0
    ,   is_declare_variables_statement tinyint unsigned default 0
    ,   is_first_statement tinyint unsigned default 0
    ) engine=MyISAM;
    
    repeat 
      set v_old_from = v_from;
      call _get_sql_token(compiled_body, v_from, v_level, v_token, 'routine', v_state);
      set _sql_tokens_id := _sql_tokens_id + 1;
      insert into _routine_tokens(id,start,level,token,state) 
      values (_sql_tokens_id, v_from - char_length(v_token), v_level, v_token, v_state);
    until 
      v_old_from = v_from
    end repeat;
  end;
  
  if debug_info then
    set debug_code := '';
    set debug_code := concat(debug_code, '_RDEBUG_RESTORE_STACK_LEVEL');
    set debug_code := concat(debug_code, '_RDEBUG_EXPORT_VARIABLES');
    set debug_code := concat(debug_code, 'call ', database(), '._rdebug_on_breakpoint(_RDEBUG_BREAKPOINT_ID,', QUOTE(rdebug_routine_schema), ',', QUOTE(rdebug_routine_name),');');
    set debug_code := concat(debug_code, '_RDEBUG_IMPORT_VARIABLES');
    set debug_code := concat(debug_code_start, _rdebug_get_debug_code_breakpoint_hint_start(), '_RDEBUG_BREAKPOINT_ID', _rdebug_get_debug_code_breakpoint_hint_end(), debug_code, debug_code_end);
    
    set first_statement_debug_code := concat('declare _rdebug_stack_level_ int unsigned default (@_rdebug_stack_level_ := ', database(), '._rdebug_get_next_stack_level());');
    set first_statement_debug_code := concat(debug_code_start, first_statement_debug_code, debug_code_end);
    
    set first_valid_for_breakpoint_debug_code := concat('call ', database(), '._rdebug_on_routine_entry(', QUOTE(rdebug_routine_schema), ',', QUOTE(rdebug_routine_name), ');');
    set first_valid_for_breakpoint_debug_code := concat(debug_code_start, first_valid_for_breakpoint_debug_code, debug_code_end);
    
    call _rdebug_analyze_routine(rdebug_routine_schema, rdebug_routine_name);
    call _rdebug_analyze_routine_params(rdebug_routine_schema, rdebug_routine_name, routine_param_list, quoting_characters);
    call _rdebug_analyze_routine_variables_scope(rdebug_routine_schema, rdebug_routine_name);

    select id from _routine_tokens where is_valid_for_breakpoint order by id limit 1 into first_valid_for_breakpoint_id;
    -- Rebuild routine body, with debug info:
    select 
        group_concat(
          if(is_first_statement, first_statement_debug_code, ''),
          if(id = first_valid_for_breakpoint_id, first_valid_for_breakpoint_debug_code, ''),
          if(
            is_valid_for_breakpoint, 
            REPLACE(
            REPLACE(
            REPLACE(
            REPLACE(
              debug_code, 
                '_RDEBUG_BREAKPOINT_ID', id ), 
                '_RDEBUG_EXPORT_VARIABLES', IF(export_variables is not null, CONCAT('set ', export_variables, ';'), '') ),
                '_RDEBUG_IMPORT_VARIABLES', IF(import_variables is not null, CONCAT('set ', import_variables, ';'), '') ),
                '_RDEBUG_RESTORE_STACK_LEVEL', 'set @_rdebug_stack_level_ := _rdebug_stack_level_;' ),
            '' -- not valid for breakpoint -- we pad nothing prior to token
          ), token 
          order by id separator '' 
        ) 
      from (
        select 
          _routine_tokens.*, 
          group_concat(variable_name order by variable_name) as variable_names,
          group_concat(if(variable_type in ('local', 'param'), concat(variable_name,':= ', '@$_$',variable_name), NULL) order by variable_name) as import_variables,
          group_concat(if(variable_type in ('local', 'param'), concat('@$_$',variable_name,':= ', variable_name), NULL) order by variable_name) as export_variables
        from 
          _routine_tokens
          left join _rdebug_routine_variables on (
            _routine_tokens.id between _rdebug_routine_variables.variable_scope_id_start and _rdebug_routine_variables.variable_scope_id_end
            and routine_schema = rdebug_routine_schema and routine_name = rdebug_routine_name
          )
        group by 
          _routine_tokens.id,
          _routine_tokens.start,
          _routine_tokens.level,
          _routine_tokens.token,
          _routine_tokens.state,
          _routine_tokens.nesting_level,
          _routine_tokens.is_valid_for_breakpoint,
          _routine_tokens.is_declare_variables_statement,
          _routine_tokens.is_first_statement
        ) select_tokens_variables
      into 
        compiled_body;
    set compiled_body_utf8 := compiled_body;
  end if;
  
  update mysql.proc set body = compiled_body, body_utf8 = compiled_body_utf8 where db = rdebug_routine_schema and name = rdebug_routine_name;

  call _rdebug_invalidate_routine_cache();
  
  set @@group_concat_max_len := @__debug_group_concat_max_len;  
END $$

DELIMITER ;

-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Return value of variable currently in active stack
-- 

DELIMITER $$

DROP function IF EXISTS rdebug_get_variable $$
CREATE function rdebug_get_variable(
    rdebug_variable_name varchar(128)
  ) returns blob
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  declare result blob default null;
  select 
      variable_value
    from 
      (select * from _rdebug_stack_state where worker_id = @_rdebug_recipient_id
       order by stack_level desc limit 1) select_current_stack_state
      join _rdebug_routine_variables using (routine_schema, routine_name)
      join _rdebug_routine_variables_state using (worker_id, stack_level, routine_schema, routine_name, variable_name)
    where
      statement_id between variable_scope_id_start and variable_scope_id_end
      and variable_name = rdebug_variable_name
    into result;
  return result;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Sent by the worker when entring a breakpoint wait 
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_release_debugger $$
CREATE procedure rdebug_release_debugger()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call _rdebug_send_breakpoint_clock();
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Step into next breakpoint in current stack level or above (does not drill in).
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_run $$
CREATE procedure rdebug_run()
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  call _rdebug_set_step_hint('run');
  -- call _rdebug_send_clock();

  -- call _rdebug_wait_for_breakpoint_clock();
  call _rdebug_release_worker_and_wait_for_breakpoint();
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Set/clear a breakpoint
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_set_breakpoint $$
CREATE procedure rdebug_set_breakpoint(
    in rdebug_routine_schema varchar(128) charset utf8,
    in rdebug_routine_name   varchar(128) charset utf8,
    in rdebug_statement_id   int unsigned,
    in rdebug_conditional_expression text charset utf8,
    in breakpoint_enabled bool
  )
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  if breakpoint_enabled then
    insert into 
      _rdebug_breakpoint_hints (worker_id, routine_schema, routine_name, statement_id, conditional_expression)
      values (@_rdebug_recipient_id, rdebug_routine_schema, rdebug_routine_name, rdebug_statement_id, rdebug_conditional_expression)
      on duplicate key update conditional_expression = values(conditional_expression)
    ;
  else
    delete from _rdebug_breakpoint_hints
      where worker_id = @_rdebug_recipient_id
        and routine_schema = rdebug_routine_schema
        and routine_name = rdebug_routine_name
        and statement_id = rdebug_statement_id
    ;
  end if;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Modify value of variable in active stack
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_set_variable $$
CREATE procedure rdebug_set_variable(
    in rdebug_variable_name varchar(128),
    in rdebug_variable_value blob
  )
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  declare current_stack_level int unsigned;
  
  select 
      max(stack_level) 
    from 
      _rdebug_stack_state 
    where 
      worker_id = @_rdebug_recipient_id
    into current_stack_level;
    
  update 
      _rdebug_stack_state
      join _rdebug_routine_variables using (routine_schema, routine_name)
      join _rdebug_routine_variables_state using (worker_id, stack_level, routine_schema, routine_name, variable_name)
    set 
      variable_value = rdebug_variable_value
    where
      _rdebug_stack_state.worker_id = @_rdebug_recipient_id
      and _rdebug_stack_state.stack_level = current_stack_level
      and _rdebug_stack_state.statement_id between variable_scope_id_start and variable_scope_id_end
      and variable_name = rdebug_variable_name
      ;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Start debugging: attach to worker (recipient)
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_set_verbose $$
CREATE procedure rdebug_set_verbose(
    in verbose bool
  )
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

BEGIN
  set @_rdebug_verbose := (verbose is true);
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_show_routine $$
CREATE procedure rdebug_show_routine(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER
COMMENT ''

BEGIN
  declare routine_body longblob default null;
  declare debug_code_start varchar(64) charset utf8 default _rdebug_get_debug_code_start();
  declare debug_code_end varchar(64) charset utf8 default _rdebug_get_debug_code_end();

  select body from mysql.proc where db = rdebug_routine_schema and name = rdebug_routine_name into routine_body;
  if routine_body is null then
    call throw(CONCAT('Unknown routine: ', mysql_qualify(rdebug_routine_schema), '.', mysql_qualify(rdebug_routine_name)));
  end if;
  
  set routine_body := replace_sections(routine_body, concat(debug_code_start, _rdebug_get_debug_code_breakpoint_hint_start()), _rdebug_get_debug_code_breakpoint_hint_end(), concat('[:\\0]', debug_code_start));
  set routine_body := replace_sections(routine_body, concat(debug_code_start, _rdebug_get_debug_code_breakpoint_hint()), debug_code_end, '[:]');
  set routine_body := replace_sections(routine_body, debug_code_start, debug_code_end, '');
  call prettify_message(concat(mysql_qualify(rdebug_routine_schema), '.', mysql_qualify(rdebug_routine_name), ' breakpoints'), routine_body);
END $$

DELIMITER ;

-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Show statement IDs for a given routine
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_show_routine_statements $$
CREATE procedure rdebug_show_routine_statements(
  in rdebug_routine_schema varchar(128) charset utf8,
  in rdebug_routine_name   varchar(128) charset utf8
  )
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  select 
      routine_schema, routine_name, statement_id
    from 
      _rdebug_routine_statements 
    where 
      routine_schema = rdebug_routine_schema
      and routine_name = rdebug_routine_name
    order by statement_id
  ;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Show visible variables in active stack
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_show_stack_state $$
CREATE procedure rdebug_show_stack_state()
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  select 
      stack_level, routine_schema, routine_name, statement_id, entry_time
    from 
      _rdebug_stack_state 
    where 
      worker_id = @_rdebug_recipient_id
    order by stack_level
  ;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Show statement at current breakpoint
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_show_statement $$
CREATE procedure rdebug_show_statement()
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  declare rdebug_start_pos, rdebug_end_pos int unsigned default 0;
  declare rdebug_statement_id int unsigned;
  declare rdebug_routine_schema, rdebug_routine_name varchar(128) default null;
  declare debug_code_start varchar(64) charset utf8 default _rdebug_get_debug_code_start();
  declare debug_code_end varchar(64) charset utf8 default _rdebug_get_debug_code_end();
  declare rdebug_routine_body longblob default null;
  
  select 
      routine_schema, routine_name, statement_id, statement_start_pos, statement_end_pos
    from 
      (select * from _rdebug_stack_state where worker_id = @_rdebug_recipient_id
       order by stack_level desc limit 1) select_current_stack_state
      join _rdebug_routine_statements using (routine_schema, routine_name, statement_id)
    into
      rdebug_routine_schema, rdebug_routine_name, rdebug_statement_id, rdebug_start_pos, rdebug_end_pos
    ;
  select 
      body 
    from 
      mysql.proc 
    where 
      db = rdebug_routine_schema and name = rdebug_routine_name 
    into rdebug_routine_body;
  if rdebug_routine_body is null then
    leave main_body;
  end if;

  set rdebug_routine_body := replace_sections(rdebug_routine_body, debug_code_start, debug_code_end, '');
  select 
    rdebug_routine_schema as routine_schema, 
    rdebug_routine_name as routine_name,
    rdebug_statement_id as statement_id,
    substring(rdebug_routine_body, rdebug_start_pos, (rdebug_end_pos - rdebug_start_pos)) as `statement`
  ;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Start debugging: attach to worker (recipient)
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_start $$
CREATE procedure rdebug_start(
  recipient_id int unsigned
  )
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER

BEGIN
  do get_lock(_rdebug_get_lock_name(connection_id(), 'debugger'), 0);
  if not get_lock(_rdebug_get_lock_name(recipient_id, 'worker'), 0) then
    do release_lock(_rdebug_get_lock_name(connection_id(), 'debugger'));
    call throw(CONCAT('Cannot obtain recipient lock for thread ', recipient_id));
  end if;
  set @_rdebug_recipient_id := recipient_id;
  set @_rdebug_worker_done := false;
  delete from _rdebug_routine_variables_state where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_stack_state where worker_id = @_rdebug_recipient_id;
  -- delete from _rdebug_breakpoint_hints where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_step_hints where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_stats where worker_id = @_rdebug_recipient_id;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Step into next breakpoint in current stack level or above (does not drill in).
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_step_into $$
CREATE procedure rdebug_step_into()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call _rdebug_set_step_hint('step_into');
  -- call _rdebug_send_clock();

  -- call _rdebug_wait_for_breakpoint_clock();
  call _rdebug_release_worker_and_wait_for_breakpoint();
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Step into next breakpoint in current stack level or above (does not drill in).
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_step_out $$
CREATE procedure rdebug_step_out()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call _rdebug_set_step_hint('step_out');
  -- call _rdebug_send_clock();

  -- call _rdebug_wait_for_breakpoint_clock();
  call _rdebug_release_worker_and_wait_for_breakpoint();
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Step into next breakpoint in current stack level or above (does not drill in).
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_step_over $$
CREATE procedure rdebug_step_over()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call _rdebug_set_step_hint('step_over');
  -- call _rdebug_send_clock();

  -- call _rdebug_wait_for_breakpoint_clock();
  call _rdebug_release_worker_and_wait_for_breakpoint();
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Clear debugging data, remove all locks.
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_stop $$
CREATE procedure rdebug_stop()
DETERMINISTIC
MODIFIES SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
  do release_lock(_rdebug_get_lock_name(connection_id(), 'debugger'));
  if @_rdebug_recipient_id is null then
    leave main_body;
  end if;
  do release_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'worker'));
  do release_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'tic'));
  do release_lock(_rdebug_get_lock_name(@_rdebug_recipient_id, 'toc'));

  delete from _rdebug_routine_variables_state where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_stack_state where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_breakpoint_hints where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_step_hints where worker_id = @_rdebug_recipient_id;
  delete from _rdebug_stats where worker_id = @_rdebug_recipient_id;
  
  set @_rdebug_recipient_id := null;
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Step into next breakpoint in current stack level or above (does not drill in).
--

DELIMITER $$

DROP procedure IF EXISTS rdebug_verbose $$
CREATE procedure rdebug_verbose()
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER

main_body: BEGIN
  call rdebug_show_stack_state();
  call rdebug_watch_variables();
  call rdebug_show_statement();
END $$

DELIMITER ;
-- 
-- This code is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, version 2
--
-- 
-- Show visible variables in active stack
-- 

DELIMITER $$

DROP procedure IF EXISTS rdebug_watch_variables $$
CREATE procedure rdebug_watch_variables()
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER

main_body: BEGIN
	
  select 
      routine_schema, routine_name, variable_name, variable_type, variable_value
    from 
      (select * from _rdebug_stack_state where worker_id = @_rdebug_recipient_id
       order by stack_level desc limit 1) select_current_stack_state
      join _rdebug_routine_variables using (routine_schema, routine_name)
      join _rdebug_routine_variables_state using (worker_id, stack_level, routine_schema, routine_name, variable_name)
    where
      statement_id between variable_scope_id_start and variable_scope_id_end
    order by 
      variable_name
    ;
END $$

DELIMITER ;



/****************************************
*****************************************
*****************************************
*****************************************
*****************************************
*****************************************
****************************************/
-- 
-- Replace sections denoted by section_start & section_end with given replacement_text
-- Text is stripped of possibly multiple occurance of section_start...some..text...section_end
-- Each such appearance is replaced with replacement_text.
-- replacement_text may include the \0 back-reference, which resovles to the text being replaced, not including boundaries.
-- 
-- Example:
-- SELECT replace_sections('<b>The</b> quick <b>brown</b> fox', '<b>', '</b>', '<span>\\0</span>')
-- Returns: '<span>The</span> quick <span>brown</span> fox'
--
--

DELIMITER $$

DROP FUNCTION IF EXISTS replace_sections $$
CREATE FUNCTION replace_sections(
  txt TEXT CHARSET utf8, 
  section_start TEXT charset utf8,
  section_end TEXT charset utf8,
  replacement_text TEXT CHARSET utf8) 
  RETURNS TEXT CHARSET utf8 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT 'Replace start-end sections with given text'

main_body: begin
  declare next_start_pos int unsigned default 1;
  declare next_end_pos int unsigned default 1;
  declare tmp_prefix, tmp_suffix, replaced_text text charset utf8;
  
  if txt is null then
    return null;
  end if;
  while true do
    set next_start_pos := LOCATE(section_start, txt, next_start_pos);
    if not next_start_pos then
      return txt;
    end if;
    set next_end_pos := LOCATE(section_end, txt, next_start_pos + char_length(section_start));
    if not next_end_pos then
      return txt;
    end if;
    set replaced_text := substring(txt, next_start_pos + char_length(section_start), (next_end_pos - next_start_pos) - char_length(section_start));
    set tmp_prefix := LEFT(txt, next_start_pos - 1);
    set tmp_suffix := SUBSTRING(txt, next_end_pos + CHAR_LENGTH(section_end));
    set txt := concat(tmp_prefix, replace(replacement_text, '\\0', replaced_text));
    set next_start_pos := char_length(txt);
    set txt := concat(txt, tmp_suffix);
  end while;
  return txt;
end $$

DELIMITER ;


DELIMITER $$

DROP procedure IF EXISTS thread_wait $$
CREATE procedure thread_wait(
    in thread_wait_name varchar(128) character set ascii, 
    in poll_seconds double,
    in atomic_execute_query text charset utf8,
    in alternate_end_wait_condition text charset utf8)
DETERMINISTIC
READS SQL DATA
SQL SECURITY INVOKER
COMMENT ''

begin
  declare current_wait_value bigint unsigned;
  
  if alternate_end_wait_condition is not null then
  	set alternate_end_wait_condition := concat('select (', alternate_end_wait_condition, ') is true into @common_schema_thread_wait_end_condition');
  end if;
  
  select 
      ifnull(max(wait_value), 0) 
    from _waits 
    where wait_name = thread_wait_name 
    into current_wait_value;
  commit;
  
  if atomic_execute_query is not null then
  	call exec_single(atomic_execute_query);
  end if;
  
  set @common_schema_thread_wait_end_condition := false;
  wait_loop: while (select ifnull(max(wait_value), 0) <= current_wait_value from _waits where wait_name = thread_wait_name) do
    if alternate_end_wait_condition is not null then
      call exec_single(alternate_end_wait_condition);
      if @common_schema_thread_wait_end_condition then
        leave wait_loop;
      end if;
    end if;  
    do sleep(poll_seconds + coalesce(0, 'common_schema_thread_wait'));
    commit;
  end while;
end $$

DELIMITER ;
-- 
-- Return a qualified MySQL name (e.g. database name, table name, column name, ...) from given text.
-- 
-- Can be used for dynamic query generation by INFORMATION_SCHEMA, where names are unqualified.
--
-- Example:
--
-- SELECT mysql_qualify('film_actor') AS qualified;
-- Returns: '`film_actor`'
-- 
DELIMITER $$

DROP FUNCTION IF EXISTS mysql_qualify $$
CREATE FUNCTION mysql_qualify(name TINYTEXT CHARSET utf8) RETURNS TINYTEXT CHARSET utf8 
DETERMINISTIC
NO SQL
SQL SECURITY INVOKER
COMMENT 'Return a qualified MySQL name from given text'

begin
  if name RLIKE '^`[^`]*`$' then
    return name;
  end if;
  return CONCAT('`', REPLACE(name, '`', '``'), '`');
end $$

DELIMITER ;
-- Outputs a prettified text message, one row per line in text
--

DELIMITER $$

DROP PROCEDURE IF EXISTS prettify_message $$
CREATE PROCEDURE prettify_message(title TINYTEXT CHARSET utf8, msg MEDIUMTEXT CHARSET utf8) 
NO SQL
SQL SECURITY INVOKER
COMMENT 'Outputs a prettified text message, one row per line in text'

main_body: begin
  declare query text charset utf8;
  
  if msg is null or msg = '' then
    leave main_body;
  end if;
  
  set @_prettify_message_text := msg;
  set @_prettify_message_num_rows := get_num_tokens(msg, '\n');
  set query := CONCAT('
    SELECT 
        split_token(@_prettify_message_text, \'\\n\', n) AS ', mysql_qualify(title), '
      FROM 
        numbers
      WHERE 
        numbers.n BETWEEN 1 AND @_prettify_message_num_rows
      ORDER BY n ASC;
    ');
  call exec_single(query);
  set @_prettify_message_text := NULL;
  set @_prettify_message_num_rows := NULL;
end $$

DELIMITER ;

delimiter //

set names utf8
//

drop procedure if exists _get_sql_token;
//

create procedure _get_sql_token(
    in      p_text      text charset utf8
,   inout   p_from      int unsigned
,   inout   p_level     int
,   out     p_token     text charset utf8
,   in      language_mode enum ('sql', 'script', 'routine')
-- ,   inout   p_state     varchar(64)charset utf8
,   inout   p_state     enum(
                            'alpha'
                        ,   'alphanum'
                        ,   'and'
                        ,   'assign'
                        ,   'bitwise and'
                        ,   'bitwise or'
                        ,   'bitwise not'
                        ,   'bitwise xor'
                        ,   'colon'
                        ,   'comma'
                        ,   'conditional comment'
                        ,   'decimal'
                        ,   'delimiter'
                        ,   'divide'
                        ,   'dot'
                        ,   'equals'
                        ,   'error'
                        ,   'greater than'
                        ,   'greater than or equals'
                        ,   'integer'
                        ,   'label'
                        ,   'left braces'
                        ,   'left parenthesis'
                        ,   'left shift'
                        ,   'less than'
                        ,   'less than or equals'
                        ,   'minus'
                        ,   'modulo'
                        ,   'multi line comment'
                        ,   'multiply'
                        ,   'not equals'
                        ,   'null safe equals'
                        ,   'or'
                        ,   'plus'
                        ,   'quoted identifier'
                        ,   'right braces'
                        ,   'right parenthesis'
                        ,   'right shift'
                        ,   'single line comment'
                        ,   'start'
                        ,   'statement delimiter'
                        ,   'string'
                        ,   'system variable'
                        ,   'user-defined variable'
                        ,   'query_script variable'
                        ,   'expanded query_script variable'
                        ,   'whitespace'
                        ,   'not'
                        )
)
comment 'Reads a token according to lexical rules for SQL'
language SQL
deterministic
no sql
sql security invoker
begin
    declare v_length int unsigned default character_length(p_text);
    declare v_no_ansi_quotes        bool default find_in_set('ANSI_QUOTES', @@sql_mode) = FALSE;
    declare v_char, v_lookahead, v_quote_char    char(1) charset utf8;
    declare v_from int unsigned;
    declare allow_script_tokens tinyint unsigned;

    set allow_script_tokens := (language_mode = 'script');

    if p_from is null then
        set p_from = 1;
    end if;
    if p_level is null then
        set p_level = 0;
    end if;
    if p_state = 'right parenthesis' then
        set p_level = p_level - 1;
    end if;
    if p_state = 'right braces' and allow_script_tokens then
        set p_level = p_level - 1;
    end if;
    set v_from = p_from;

    set p_token = ''
    ,   p_state = 'start';

    my_loop: while v_from <= v_length do
        set v_char = substr(p_text, v_from, 1)
        ,   v_lookahead = substr(p_text, v_from+1, 1)
        ;

        state_case: begin case p_state
            when 'error' then
                set p_from = v_length;
                leave state_case;
            when 'start' then
                case
                    when v_char between '0' and '9' then
                        set p_state = 'integer';
                    when v_char between 'A' and 'Z'
                    or   v_char between 'a' and 'z'
                    or   v_char = '_' then
                        set p_state = 'alpha';
                    when v_char = ' ' then
                        set p_state = 'whitespace'
                        ,   v_from = v_length - character_length(ltrim(substring(p_text, v_from)))
                        ;
                        leave state_case;
                    when v_char in ('\t', '\n', '\r') then
                        set p_state = 'whitespace';
                    when v_char = '''' or v_no_ansi_quotes and v_char = '"' then
                        set p_state = 'string', v_quote_char = v_char;
                    when v_char = '`' or v_no_ansi_quotes = FALSE and v_char = '"' then
                        set p_state = 'quoted identifier', v_quote_char = v_char;
                    when v_char = '@' then
                        if v_lookahead = '@' then
                            set p_state = 'system variable', v_from = v_from + 1;
                        else
                            set p_state = 'user-defined variable';
                            if v_lookahead = '''' then
                                set v_from = v_from + 1;
                                leave my_loop;
                            end if;
                        end if;
                    when v_char = '$' and allow_script_tokens then
                        set p_state = 'query_script variable';
                    when v_char = '.' then
                        if substr(p_text, v_from + 1, 1) between '0' and '9' then
                            set p_state = 'decimal', v_from = v_from + 1;
                        else
                            set p_state = 'dot', v_from = v_from + 1;
                            leave my_loop;
                        end if;
                    when v_char = ';' then
                        set p_state = 'statement delimiter', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = ',' then
                        set p_state = 'comma', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '=' then
                        set p_state = 'equals', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '*' then
                        set p_state = 'multiply', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '%' then
                        set p_state = 'modulo', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '/' then
                        if v_lookahead = '*' then
                            set v_from = locate('*/', p_text, p_from + 2);
                            if v_from then
                                set p_state = if (substr(p_text, p_from + 2, 1) = '!', 'conditional comment', 'multi line comment')
                                ,   v_from = v_from + 2
                                ;
                                leave my_loop;
                            else
                                set p_state = 'error';
                            end if;
                        else
                            set p_state = 'divide', v_from = v_from + 1;
                            leave my_loop;
                        end if;
                    when v_char = '-' then
                        case
                            when v_lookahead = '-' and substr(p_text, v_from + 2, 1) = ' ' then
                                set p_state = 'single line comment'
                                ,   v_from = locate('\n', p_text, p_from)
                                ;
                                if not v_from then
                                    set v_from = v_length;
                                end if;
                                set v_from = v_from + 1;
                                leave my_loop;
                            else
                                set p_state = 'minus', v_from = v_from + 1;
                                leave my_loop;
                        end case;
                    when v_char = '#' then
                        set p_state = 'single line comment'
                        ,   v_from = locate('\n', p_text, p_from)
                        ;
                        if not v_from then
                            set v_from = v_length;
                        end if;
                        set v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '+' then
                        set p_state = 'plus', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '<' then
                        set p_state = 'less than';
                    when v_char = '>' then
                        set p_state = 'greater than';
                    when v_char = ':' then
                        if v_lookahead = '=' then
                            set p_state = 'assign', v_from = v_from + 2;
                            leave my_loop;
                        elseif v_lookahead = '$' and allow_script_tokens then
                            set p_state = 'expanded query_script variable';
                        else
                            set p_state = 'colon', v_from = v_from + 1;
                            leave my_loop;
                        end if;
                    when v_char = '{' and allow_script_tokens then
                        set p_state = 'left braces', v_from = v_from + 1, p_level = p_level + 1;
                        leave my_loop;
                    when v_char = '}' and allow_script_tokens then
                        set p_state = 'right braces', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '(' then
                        set p_state = 'left parenthesis', v_from = v_from + 1, p_level = p_level + 1;
                        leave my_loop;
                    when v_char = ')' then
                        set p_state = 'right parenthesis', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '^' then
                        set p_state = 'bitwise xor', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '~' then
                        set p_state = 'bitwise not', v_from = v_from + 1;
                        leave my_loop;
                    when v_char = '!' then
                        if v_lookahead = '=' then
                            set p_state = 'not equals', v_from = v_from + 2;
                        else
                            set p_state = 'not', v_from = v_from + 1;
                        end if;
                        leave my_loop;
                    when v_char = '|' then
                        if v_lookahead = '|' then
                            set p_state = 'or', v_from = v_from + 2;
                        else
                            set p_state = 'bitwise or', v_from = v_from + 1;
                        end if;
                        leave my_loop;
                    when v_char = '&' then
                        if v_lookahead = '&' then
                            set p_state = 'and', v_from = v_from + 2;
                        else
                            set p_state = 'bitwise and', v_from = v_from + 1;
                        end if;
                        leave my_loop;
                    else
                        set p_state = 'error';
                end case;
            when 'less than' then
                case v_char
                    when '=' then
                        set p_state = 'less than or equals';
                        leave state_case;
                    when '>' then
                        set p_state = 'not equals';
                    when '<' then
                        set p_state = 'left shift';
                    else
                        do null;
                end case;
                leave my_loop;
            when 'less than or equals' then
                if v_char = '>' then
                    set p_state = 'null safe equals'
                    ,   v_from = v_from + 1
                    ;
                end if;
                leave my_loop;
            when 'greater than' then
                case v_char
                    when '=' then
                        set p_state = 'greater than or equals';
                    when '>' then
                        set p_state = 'right shift';
                    else
                        set p_state = 'error';
                end case;
                leave my_loop;
            when 'multi line comment' then
                if v_char = '*' and v_lookahead = '/' then
                    set v_from = v_from + 2;
                    leave my_loop;
                end if;
            when 'alpha' then
                case
                    when v_char between 'A' and 'Z'
                    or   v_char between 'a' and 'z'
                    or   v_char = '_' then
                        leave state_case;
                    when v_char between '0' and '9'
                    or   v_char = '$' then
                        set p_state = 'alphanum';
                    else
--                        if v_char = ':' and v_lookahead not in ('=', '$') then
--                          set p_state = 'label', v_from = v_from + 1;
--                        end if;
                        leave my_loop;
                end case;
            when 'alphanum' then
                case
                    when v_char between 'A' and 'Z'
                    or   v_char between 'a' and 'z'
                    or   v_char = '_'
                    or   v_char between '0' and '9' then
                        leave state_case;
                    else
--                        if v_char = ':' and v_lookahead not in ('=', '$') then
--                          set p_state = 'label', v_from = v_from + 1;
--                        end if;
                        leave my_loop;
                end case;
            when 'integer' then
                case
                    when v_char between '0' and '9' then
                        leave state_case;
                    when v_char = '.' then
                        set p_state = 'decimal';
                    else
                        leave my_loop;
                end case;
            when 'decimal' then
                case
                    when v_char between '0' and '9' then
                        leave state_case;
                    else
                        leave my_loop;
                end case;
            when 'whitespace' then
                if v_char not in ('\t', '\n', '\r') then
                    leave my_loop;
                end if;
            when 'string' then
                -- find the closing quote
                set v_from = locate(v_quote_char, p_text, v_from);
                if v_from then  -- found a closing quote
                    if substr(p_text, v_from - 1, 1) = '\\' then
                        -- this quote was preceded by a backslash.
                        -- we now have to figure out if this was an escaping backslash.
                        backslahses: begin
                            declare v_backslash int unsigned default v_from - 2;
                            while substr(p_text, v_backslash, 1) = '\\' do
                                -- we found 2 consecutive backslashes.
                                -- see if there are even more:
                                if substr(p_text, v_backslash - 1, 1) = '\\' then
                                    -- more backslashes, continue the loop.
                                    set v_backslash = v_backslash - 2;
                                else
                                    -- no more backslases.
                                    -- The quote was not escaped by a backslash.
                                    leave backslahses;
                                end if;
                            end while;
                            -- if we arrive here, the backslash escaped the quote.
                            -- this means we haven't found the end of the string yet.
                            -- so, we have to look beyond the quote for a new one.
                            set v_from = v_from + 1;
                            if v_from > v_length then
                                set p_state = 'error';
                                leave my_loop;
                            else
                                iterate my_loop;
                            end if;
                        end backslahses;
                    end if;

                    -- by now we established that the quote was not escaped by a preceding backslash.
                    -- but it could still be escaped by a following quote char.
                    if substr(p_text, v_from + 1, 1) = v_quote_char then
                        -- this quote is followed by the same quote,
                        -- this means it was an escaped quote.
                        -- so, continue beyond this point to find the real end of the string.
                        set v_from = v_from + 2;
                        if v_from > v_length then
                            set p_state = 'error';
                            leave my_loop;
                        else
                            iterate my_loop;
                        end if;
                    else
                        -- ok, this quote appears to be the real end of the string.
                        -- leave to produce the string token.
                        set v_from = v_from + 1;
                        leave my_loop;
                    end if;
                else  -- no closing quote found. This must be an error.
                    set p_state = 'error', v_from = v_length;
                    leave my_loop;
                end if;
            when 'quoted identifier' then
                if v_char != v_quote_char then
                    leave state_case;
                else
                    set v_from = v_from + 1;
                    leave my_loop;
                end if;
            when 'user-defined variable' then
                if v_char in (';', ',', ' ', '\t', '\n', '\r', '!', '~', '^', '%', '>', '<', ':', '=', '+', '-', '&', '*', '|', '(', ')') then
                    leave my_loop;
                elseif allow_script_tokens and v_char in ('{', '}') then
                    leave my_loop;
                end if;
            when 'query_script variable' then
                if v_char in (';', ',', ' ', '\t', '\n', '\r', '!', '~', '^', '%', '>', '<', ':', '=', '+', '-', '&', '*', '|', '(', ')') then
                    leave my_loop;
                elseif allow_script_tokens and v_char in ('{', '}', '.') then
                    leave my_loop;
                end if;
            when 'expanded query_script variable' then
                if v_char in (';', ',', ' ', '\t', '\n', '\r', '!', '~', '^', '%', '>', '<', ':', '=', '+', '-', '&', '*', '|', '(', ')') then
                    leave my_loop;
                elseif allow_script_tokens and v_char in ('{', '}', '.') then
                    leave my_loop;
                end if;
            when 'system variable' then
                if v_char in (';', ',', ' ', '\t', '\n', '\r', '!', '~', '^', '%', '>', '<', ':', '=', '+', '-', '&', '*', '|', '(', ')') then
                    leave my_loop;
                elseif allow_script_tokens and v_char in ('{', '}') then
                    leave my_loop;
                end if;
            else
                leave my_loop;
        end case; end state_case;
        set v_from = v_from + 1;
    end while my_loop;
    set p_token = substr(p_text, p_from, v_from - p_from) collate utf8_general_ci;
    set p_from = v_from;
end;
//

delimiter ;