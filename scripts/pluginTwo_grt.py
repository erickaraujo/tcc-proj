# MySQL Workbench Plugin
# <description>
# Written in MySQL Workbench 6.3.9

from wb import *
import grt
import mforms

from text_output import TextOutputTab
from run_script import RunScriptForm

ModuleInfo = DefineModule(name='PluginTwo', author="Author Name", version="1.0", description="Contains Plugin pluginTwo")

# This plugin takes the selected text in a SQL text sditor and replaces it with the returned value
@ModuleInfo.plugin('ifam.tcc.pluginTwo', caption="Reption", input=[wbinputs.currentQueryEditor()], pluginMenu = "SQL/Utilities")
@ModuleInfo.export(grt.INT, grt.classes.db_query_QueryEditor)
def commentText(editor):
    commentType = "%s " % grt.root.wb.options.options["DbSqlEditor:SQLCommentTypeForHotkey"]
    commentTypeLength = len(grt.root.wb.options.options["DbSqlEditor:SQLCommentTypeForHotkey"]) + 1
    text = editor.selectedText
    original_text = editor.selectedText
    if text:
        lines = text.split("\n")
        if lines[0].startswith(commentType):
            new_text = "\n".join((line[commentTypeLength:] if line.startswith(commentType) else line) for line in lines)
        else:
            new_text = "\n".join(commentType + line for line in lines)
        editor.replaceSelection(new_text)
    else:
        pos = editor.insertionPoint
        full_text = editor.script
        done = False
        # if cursor is before or after a comment sequence, then delete that
        if full_text[pos:pos+commentTypeLength] == commentType:
            editor.replaceContents(full_text[:pos] + full_text[pos+commentTypeLength:])
            done = True
        else:
            for i in range(4):
                if full_text[pos+i:pos+i+commentTypeLength] == commentType:
                    editor.replaceContents(full_text[:pos+i] + full_text[pos+i+commentTypeLength:])
                    done = True
                    break
                if pos-i >= 0 and full_text[pos-i:pos-i+commentTypeLength] == commentType:
                    editor.replaceContents(full_text[:pos-i] + full_text[pos-i+commentTypeLength:])
                    done = True
                    pos -= i
                    break
        
        if not done:
            editor.replaceSelection(commentType)

        editor.insertionPoint = pos

    return 0

# This plugin run the selected text and show a output value
@ModuleInfo.plugin('ifam.tcc.pluginThree', caption="Rep2", input=[wbinputs.currentQueryEditor()], pluginMenu = "SQL/Utilities")
@ModuleInfo.export(grt.INT, grt.classes.db_query_QueryEditor)
def newFunction(editor):
    
    statement = "select 'test' as '' "

    output = ["Execute:"]

    if statement:
        resultset = editor.owner.executeScript(statement)


        view = TextOutputTab('\n'.join(output) + ' \n 2nd test')

        dock = mforms.fromgrt(qbuffer.resultDockingPoint)
        dock.dock_view(view, '', 0)
        dock.select_view(view)
        view.set_title("Query Output")
        
    return 0