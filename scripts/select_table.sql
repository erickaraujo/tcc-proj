set @schema := '';

SELECT db 'database', name, CONVERT(param_list USING utf8) 'param_list', CONVERT(body_utf8 USING utf8)'text' FROM mysql.proc
where type = 'PROCEDURE'
AND (((@schema = '' or @schema is null) and db NOT IN ('common_schema', 'sys')) OR (db = @schema));