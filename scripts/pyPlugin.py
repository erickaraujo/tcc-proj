# coding=utf-8

# ^initializing this script with codification via Python interpreter
from wb import *
from workbench.utils import Version
from sql_reformatter import formatter_for_statement_ast
from workbench.log import log_error
from run_script import RunScriptForm
from workbench.db_utils import MySQLConnection, MySQLError, MySQLResult
from __builtin__ import any as b_any

import grt
import mforms
import sqlide_grt, db_mysql_fe_grt
import os
import sys
import shutil
import traceback
import re
import threading
from collections import OrderedDict

""" annotations 
---------------
    mforms.newBox(bool horizontal)
"""

# define this Python module as a GRT module
ModuleInfo = DefineModule(name="SPDebugger", author= "Erick Araujo", version="1.0")

# @wbexport exports the function from the module and also describes the return and argument types of the function
# @wbplugin defines the name of the plugin to "br.com.tcc.PyPlugin", sets the caption to be 
# shown in places like the menu, where to take input arguments from and also that it should be included
# in the Utilities submenu in Tools menu.
@ModuleInfo.plugin("br.ifam.tcc.SPDebugger", caption= "Open Stored Procedure Debugger", input=[wbinputs.currentQueryEditor(), wbinputs.currentSQLEditor(), wbinputs.currentSQLEditor()], pluginMenu= "SQL/Utilities", type="standalone")
@ModuleInfo.export(grt.INT, grt.classes.db_query_QueryEditor, grt.classes.db_query_Editor, grt.classes.db_mgmt_Connection)
def mainForm(current_query_editor, current_sql_editor, connection):
    form = InitializeChooser(current_query_editor, current_sql_editor, connection)

class InitializeChooser(mforms.Form):
    def __init__(self,  query_editor, sql_editor, connection):
        self.editor = query_editor
        self.sql_editor = sql_editor

        """There are declarations of 'schema_name' variable for each class, watchout!"""
        self.schema_name = grt.root.wb.sqlEditors[0].defaultSchema

        chooser_main_form = mforms.Form(None, mforms.FormSingleFrame|mforms.FormResizable|mforms.FormMinimizable)
        chooser_main_form.set_title("PyPlugin Debugger Tool")

        box_main_form = mforms.newBox(False)
        box_main_form.set_padding(12)
        box_main_form.set_spacing(8)

        table = mforms.newTable()
        table.set_padding(20)
        table.set_row_count(2)
        table.set_column_count(3)
        table.set_row_spacing(8)
        table.set_column_spacing(4)
        """ table.add parameters
            table.add(View view, INT row_left, INT row_right, INT column_top, INT column_bottom, INT flags) """
        table.add(mforms.newLabel("Default Schema:"), 0, 1, 0, 1, 0)
        self.textentry_schema = mforms.newTextEntry(mforms.NormalEntry)
        self.textentry_schema.set_value(self.schema_name)
        self.textentry_schema.set_read_only(True)
        table.add(self.textentry_schema, 1, 2, 0, 1, mforms.HFillFlag|mforms.HExpandFlag)

        table.add(mforms.newLabel("Choose a Stored Procedure:"), 0, 1, 1, 2, 0)
        self.stored_procs = mforms.newSelector(mforms.SelectorCombobox)
        self.init_stored_procs()
        table.add(self.stored_procs, 1, 2, 1, 2, mforms.HFillFlag|mforms.HExpandFlag)

        box_main_form.add(table, False, True)
        self.ok = mforms.newButton()
        self.ok.set_text("Choose")
        
        self.stored_procedure_object = [""]

        self.cancel = mforms.newButton()
        self.cancel.set_text("Cancel")

        self.cancel.add_clicked_callback(chooser_main_form.close)

        btnbox = mforms.newBox(True)
        btnbox.set_spacing(8)
        mforms.Utilities.add_end_ok_cancel_buttons(btnbox, self.ok, self.cancel)

        box_main_form.add_end(btnbox, False, True)

        chooser_main_form.center()
        chooser_main_form.set_content(box_main_form)
        chooser_main_form.set_size(500, 200)
        if not self.stored_procs.get_string_value().encode("utf-8"):
            mforms.Utilities.show_warning("Error", "No stored procedures were found in this actual schema <" + self.schema_name + ">!",
             "OK", "", "")
            return None

        if chooser_main_form.run_modal(self.ok, self.cancel):
            self.get_stored_proc()
            editor_form = EditorForm(self.editor, self.sql_editor, self.stored_procedure_object, connection)

    def init_stored_procs(self):
        sch = "'" + self.schema_name + "'"
        script = "SELECT name AS 'sp' FROM mysql.proc WHERE db NOT IN  ('common_schema', 'common_schema_version_control') and type = 'PROCEDURE' AND db = " + sch + ";"
        result = grt.root.wb.sqlEditors[0].executeQuery(script, 0)
        sps = []
        if result:
            while result.nextRow():
                sps.append(result.stringFieldValue(0))
            self.stored_procs.add_items(sps)

    def get_stored_proc(self):
        sch = self.schema_name
        stored_procedure = str(self.stored_procs.get_string_value().encode("utf-8"))
        script = "SHOW CREATE PROCEDURE {0}.{1}".format(sch, stored_procedure)
        result = grt.root.wb.sqlEditors[0].executeQuery(script, 0)
        name = ""
        body = ""
        if result:
            while result.nextRow():
                name = result.stringFieldValue(0)
                body = result.stringFieldValue(2)
        self.stored_procedure_object.append(name)
        self.stored_procedure_object.append(body)

###############################################################################
###############################################################################
###############################################################################


class EditorForm(mforms.Form):
    def __init__(self, current_query_editor, current_sql_editor, stored_procedure_object, connection):
        try:
            self.schema_name = grt.root.wb.sqlEditors[0].defaultSchema
            self.stored_procedure_name = stored_procedure_object[1]
            self.stored_procedure_body = stored_procedure_object[2]
            self.connection = connection
            self.current_query_editor = current_query_editor    
            self.current_sql_editor = current_sql_editor
            self.window_main_form = mforms.Form(None, mforms.FormResizable)
            self.window_main_form.set_title('Debugger Tool')
            self.__update_timer = None
            self.__dthread_running = False
            self.__wthread_running = False
            self.__debug_running = False
            self.__verbose_debug = True

            """ box_main_form """
            box_main_form = mforms.newBox(False)
            box_main_form.set_padding(5)
            box_main_form.set_spacing(5)
            ######################################

            box_container = mforms.newBox(True)
            box_container.set_spacing(15)
            box_container.set_padding(10)
            ######################################

            panel_code_editor = mforms.newPanel(mforms.TitledBoxPanel)
            panel_code_editor.set_title('Stored Procedure')
            box_code_editor = mforms.newBox(True)
            box_code_editor.set_padding(2)
            panel_code_editor.add(box_code_editor)

            ######################################

            """set_size(width, height)"""
            """set_size(largura, altura)"""

            self.code_editor = mforms.newCodeEditor()
            self.code_editor.set_size(550, 450)
            self.code_editor.set_language(mforms.LanguageMySQL)
            self.code_editor.set_text(self.stored_procedure_body)
            box_code_editor.add(self.code_editor, True, True)
            ######################################

            panel_output = mforms.newPanel(mforms.TitledBoxPanel)
            panel_output.set_title('Output')
            box_debugger = mforms.newBox(True)
            box_debugger.set_size(350, 450)
            box_debugger.set_padding(2)
            self.textbox_output = mforms.newTextBox(mforms.BothScrollBars)
            self.textbox_output.set_read_only(True)
            self.textbox_output.set_padding(5)
            box_debugger.add(self.textbox_output, True, True)
            panel_output.add(box_debugger)
            ######################################

            toolbar = mforms.newToolBar(mforms.SecondaryToolBar)            
            ti_run_debug = mforms.newToolBarItem(mforms.ActionItem)
            """ icons must be 18x18 size """
            ti_run_debug.set_icon(os.getcwd()+"\\images\\icons\\debug_continue.png")
            ti_run_debug.set_tooltip('Button start')
            # ti_run_debug.add_activated_callback(self.to_do_action_buttons)
            # ti_run_debug.add_activated_callback(self.rdebug_run)
            
            ti_stop_debug = mforms.newToolBarItem(mforms.ActionItem)
            ti_stop_debug.set_icon(os.getcwd()+"\\images\\icons\\debug_stop.png")
            ti_stop_debug.set_tooltip('Button stop')
            ti_stop_debug.add_activated_callback(self.clear_output)
            
            ti_breakpoint = mforms.newToolBarItem(mforms.ActionItem)
            ti_breakpoint.set_icon(os.getcwd()+"\\images\\icons\\query_stop_on_error.png")
            ti_breakpoint.set_tooltip('Set a breakpoint on line-cursor position')
            ti_breakpoint.add_activated_callback(self.add_remove_breakpoint)
            
            ti_step_into = mforms.newToolBarItem(mforms.ActionItem)
            ti_step_into.set_icon(os.getcwd()+"\\images\\icons\\debug_step_into.png")
            ti_step_into.set_tooltip('Step into')
            ti_step_into.add_activated_callback(self.rdebug_step_into)
            
            ti_step_out = mforms.newToolBarItem(mforms.ActionItem)
            ti_step_out.set_icon(os.getcwd()+"\\images\\icons\\debug_step_out.png")
            ti_step_out.set_tooltip('Step out')
            ti_step_out.add_activated_callback(self.to_do_action_buttons)
            
            ti_step_over = mforms.newToolBarItem(mforms.ActionItem)
            ti_step_over.set_icon(os.getcwd()+"\\images\\icons\\debug_step.png")
            ti_step_over.set_tooltip('Step over')
            ti_step_over.add_activated_callback(self.to_do_action_buttons)

            toolbar.add_item(ti_run_debug)
            toolbar.add_item(ti_stop_debug)
            toolbar.add_item(mforms.newToolBarItem(mforms.SeparatorItem))
            toolbar.add_item(ti_breakpoint)
            toolbar.add_item(ti_step_into)
            toolbar.add_item(ti_step_out)
            toolbar.add_item(ti_step_over)
            ######################################


            btn_cancel = mforms.newButton()
            btn_cancel.set_text('Cancel')
            btn_cancel.add_clicked_callback(self.window_main_form.close)
            button_box = mforms.newBox(True)
            button_box.set_padding(10)
            button_box.add_end(btn_cancel, False, True)
            ######################################
            
            """ initializing form structure """
            box_container.add(panel_code_editor, False, True)
            box_container.add(panel_output, True, True)
            box_main_form.add(toolbar, True, False)
            box_main_form.add(box_container, True, False)
            box_main_form.add_end(button_box, False, False)
            ######################################

            if self.check_common_schema():
                self.add_compiled_debug()
                self._list_parameters = []
                ######################################

                self.window_main_form.set_content(box_main_form)
                self.window_main_form.show()
                self.window_main_form.center()
                self.window_main_form.add_closed_callback(self.close_window)
            else:
                mforms.Utilities.show_warning("Erro ao importar common_schema!", ""+str(traceback.format_exc()), "OK", "", "")

        except:
            mforms.Utilities.show_warning("Erro! 2nd try-except", ""+str(traceback.format_exc()), "OK", "", "") 
            raise

    def check_common_schema(self):
        script = "select case when exists( select 1 from information_schema.SCHEMATA where schema_name = 'common_schema') then 'True' else 'False' end 'exists' ;"
        result_set = grt.root.wb.sqlEditors[0].executeQuery(script, 0)
        if result_set:
            while result_set.nextRow():
                result = result_set.stringFieldValue(0)
        if result == "True":
            pass 
        else:
            mforms.Utilities.show_warning("Exception!", "common_schema not found! Initializing import form...", "OK", "", "")
            self.init_common_schema()

    def init_common_schema(self):
        try:
            #return root directory+home plugin directory Windows: 'C:\pyplugin\common_schema.sql', need to test in linux/mac
            dir_origin = os.path.abspath(os.sep)+'pyplugin\\common_schema.sql' 

            #directory path of actual file 'workbench\modules\pyPlugin_v2_grt.py
            dir_dest = os.path.abspath(os.path.dirname(__file__)) 

            if self.current_sql_editor.serverVersion.majorNumber > 5 or (self.current_sql_editor.serverVersion.majorNumber == 5 and self.current_sql_editor.serverVersion.minorNumber >= 1):
                pass
            else:
                mforms.Utilities.show_warning("MySQL Version Error!", "Actual version " + 
                        str(self.current_sql_editor.serverVersion.majorNumber) + "." + 
                        str(self.current_sql_editor.serverVersion.minorNumber) + " is incompatible. \nWe recommend MySQL Version 5.1 or newer",
                        "OK", "", "")
                return False

            #################################################################################################
            #################################  FIX ME OR IMPROVE ME #########################################
            #################################################################################################
            
            #Instead of a fixed filename, put a regex to find a 'common_schema-2.3.sql' for e.g
            if os.path.isfile(dir_dest+"\\common_schema.sql"): 
                pass
            elif os.path.isfile(dir_origin) and os.path.isdir(dir_dest):
                shutil.copy(dir_origin, dir_dest)
                # mforms.Utilities.show_warning("Success","Copied with success to " + dir_dest,"OK","","")
            else:
                #fix this error message, to clarify the user
                mforms.Utilities.show_warning("Error in directory!","dir1? " + dir_origin + " -> " + str(os.path.isfile(dir_origin)) + 
                "\n dir2? " + dir_dest + " -> " + str(os.path.isdir(dir_dest)),"OK","","")
                return False
                
            path = os.path.abspath(os.path.dirname(__file__))+"\\common_schema.sql"
            
            try:
                #import via RunScriptForm and need the user to click more 2 times or import everything silently? 
                # (reading everything in file and then executeScript/executeQuery ?) <- FAIL
                # (implement a progress_bar to wait for common_schema install?) <- NOT TESTED YET 
                form = RunScriptForm(self.current_sql_editor)
                success_install = form.run()
                # form.run_file(path)
                return success_install
            except:
                    mforms.Utilities.show_warning("Erro!", ""+str(traceback.format_exc()), "OK", "", "")
                    return False
            #################################################################################################
            ###############################  END FIX ME OR IMPROVE ME #######################################
            #################################################################################################

        except:
                mforms.Utilities.show_warning("Erro 1st try!", ""+str(traceback.format_exc()), "OK", "", "") 
                return False

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    """ UI Related """
    def print_output(self, text):
        ## if print_output function is called alone without text they will be overwritten by the self object call
        if text:
            self.textbox_output.append_text_and_scroll("APP>> " + str(text) +"\n", True)

    def __set_text_output(self, text):
        self.textbox_output.append_text_and_scroll(text, True)

    def __get_text_output(self):
        return self.textbox_output.get_string_value()

    def clear_output(self, clear):
        self.textbox_output.clear()

    def close_window(self):
        self.remove_compiled_debug()
        if self.__update_timer:
            mforms.Utilities.cancel_timeout(self.__update_timer)

    """ Only used for app debug purpose """
    def __verbose_print_output(self, text):
        if text and self.__verbose_debug:
            self.textbox_output.append_text_and_scroll("(DEBUG) >> " + str(text) +"\n", True)

    def print_formatted_text(self, list_results):
        statement_number = 1
        result_text = "Result: \n"
        identation = " " * 5
        linebreak = "\n"
        column_separator = " | "
        column_point = "+ "
        row_pointer = "> "
        
        for result in list_results:
            try:
                row_line = "-"*(len(identation)*3)
                result_text += column_point + row_line + linebreak
                result_text += column_separator + "SELECT " + str(statement_number) + linebreak  
                for c in range(result.numFields()):
                    c+=1
                    i=1
                    column_name = " Column: " + result.fieldName(c)
                    result_text += column_point + row_line + linebreak
                    result_text += column_separator + column_name + linebreak
                    result_text += column_point + row_line + linebreak
                    flag = result.firstRow()
                    result_text += column_separator + ("Row(s): ") + linebreak
                    while flag:
                        row = column_separator + (str(i)+"-") + row_pointer + result.stringByName(result.fieldName(c)) + linebreak
                        result_text += row
                        flag = result.nextRow()
                        i+=1
                result_text += column_point + row_line + linebreak + linebreak + linebreak
                statement_number+=1
            except:
                self.print_output("0 row(s) returned")
                result_text = None

        if result_text:
            self.print_output(result_text)

    def __update_ui(self):
        txt = self.__get_text_output()
        self.clear_output('')

        if self.__dthread_event.isSet() or self.__wthread_event.isSet():
            self.__set_text_output(txt)

        return True

    def to_do_action_buttons(self, s):
        try:
            mforms.Utilities.show_warning("Buttons Clicked", "TO DO FUNCTIONS", "OK", "", "")
            self.__verbose_print_output('"to do action buttons" clicked')
        except:
            raise

    """ Method to be setted in toolbar action """
    def add_remove_breakpoint(self, bp):
        self.__add_remove_breakpoint(None)

    """ Check breakpoints from rdebug_breakpoints and then printing them in self.code_editor, allowing to add/remove """
    def __add_remove_breakpoint(self, breakpoint_tagline):
        if breakpoint_tagline:
            position = breakpoint_tagline
            ret, line, error = self.__search_breakpoint(position)
            if ret:
                self.__set_breakpoint(line)
            else:
                self.print_output(error)
        else:
            line = self.code_editor.line_from_position(self.code_editor.get_caret_pos()) #code editor line is 0 based        
            self.__set_breakpoint(line)
        
    def __search_breakpoint(self, position):
        script = "SHOW CREATE PROCEDURE {0}.{1}".format(self.schema_name, self.stored_procedure_name)
        words = "/*[B:{0}:]*/".format(position)
        first_line_pos = self.code_editor.line_from_position(self.code_editor.get_caret_pos())
        # self.print_output("word position_name >> {0}".format(words))
        temp_editor = mforms.newCodeEditor()
        temp_editor.set_language(mforms.LanguageMySQL)
        try:
            result = self.debug_execute_script(script)
            if result and result.nextRow():
                temp_editor.set_text(result.stringByName("Create Procedure"))
                temp_editor.find_and_highlight_text(words, mforms.FindDefault, False, False)
                actual_line_pos = temp_editor.line_from_position(temp_editor.get_caret_pos())-1
                # self.print_output("actual_line_pos >> {0} -- try".format(actual_line_pos))
                self.code_editor.set_selection(temp_editor.position_from_line(actual_line_pos), 0)
                if first_line_pos == actual_line_pos:
                    temp_editor.find_and_highlight_text(words, mforms.FindDefault, False, True)
                    actual_line_pos = temp_editor.line_from_position(temp_editor.get_caret_pos())
                    # self.print_output("actual_line_pos >> {0} -- try-if".format(actual_line_pos))
                    self.code_editor.set_selection(temp_editor.position_from_line(actual_line_pos), 0)

                ret = True
                error = None
                position = actual_line_pos
        except:
            ret = False
            error = "Error on find the selected breakpoint"
            position = None
        
        temp_editor = None
        return [ret, position, error]

    def __set_breakpoint(self, line):
        if self.code_editor.has_markup(mforms.LineMarkupBreakpoint, line):
            self.code_editor.remove_markup(mforms.LineMarkupBreakpoint, line) 
            # self.print_output("Removed Breakpoint in line " + str(line+1))
        else:
            self.code_editor.show_markup(mforms.LineMarkupBreakpoint, line) 
            # self.print_output("Added Breakpoint from line " + str(line+1))

    """ INPUT FORM FOR PARAMETERS """

    def __form_parameters(self):
        param_form = mforms.Form(None, mforms.FormToolWindow)
        param_form.set_title("Parameters")
        param_box = mforms.newBox(False)
        param_box.set_spacing(8)
        param_box.set_padding(8)

        table = mforms.newTable()
        table.set_padding(10)
        table.set_row_spacing(8)
        table.set_column_spacing(2)
        table.set_column_count(3)


        """
       ---------------------------------------
        EXAMPLE FOR TABLE.ADD
       --------------------------------------- 
            table.add parameters
            table.add(
                      View view, 
                      INT column left,  #left column to put the item in 
                      INT column right, #column that the item ends at (ie at least left+1)
                      INT row top,      #top row to put the item in
                      INT row bottom,   #row that the item ends at (ie at least top+1)
                      INT flags ={ 
                          mforms.NoFillExpandFlag (0)   #Neither expand nor fill.
                          mforms.HExpandFlag (1 << 0),  #same for horizontal filling
                          mforms.VExpandFlag (1 << 1),  #whether item should try using vertical space leftover from table
                          mforms.HFillFlag (1 << 2),    #same for horizontal expansion
                          mforms.VFillFlag (1 << 3)     #whether all allocated vertical space should be filled, or just centered in it
                      }) 
       --------------------------------------
                      """

        regex = '(\w+);;(\w+);;(\w+)'
        pat = re.compile(regex, re.IGNORECASE)
        dict_textentry = {}
        dict_params = OrderedDict()
        dict_text_out = OrderedDict()
        dict_text_in = OrderedDict()
        
        if self.__append_parameters_to_list():
            self.__verbose_print_output("DEBUG FORM_PARAM 'IF APPEND_TO_PARAM IS TRUE' --> entered in 'append_parameters_to_list()'")
            left = 0 
            top = 0
            right = 1
            bottom = 1
            table.set_row_count(len(self._list_parameters))
            for param in self._list_parameters:
                m = re.match(regex, param)
                self.__verbose_print_output("DEBUG 'FOR PARAM IN SELF_LIST' --- >  m.group(1) -> " + m.group(1))

                if m.group(1) == "IN":
                    self.__verbose_print_output("DEBUG 'if group(1) = in' ---> entered")
                    
                    param_name = mforms.newLabel(str(m.group(1))+" " + str(m.group(2)))
                    param_name.set_text_align(mforms.NoAlign)
                    param_textbox = mforms.newTextEntry(mforms.NormalEntry)
                    param_textbox.set_size(125, 20)
                    param_type = mforms.newLabel("("+str(m.group(3))+")")
                    """adding each param_textbox variable in a dictionary for use later"""
                    dict_textentry[m.group(2)] = param_textbox
                    dict_params[m.group(2)] = m.group(2)
                    table.add(param_name, 0, 1, top, bottom, 0)
                    table.add(param_textbox, 1, 2, top, bottom, 0)
                    table.add(param_type, 2, 3, top, bottom, 0)
                    top +=1
                    bottom +=1
                    """add "in" parameters"""
                    dict_text_in[m.group(2)] = param_textbox

                elif m.group(1) == "OUT":
                    """add "out" parameters who'll return values"""
                    self.__verbose_print_output("DEBUG 'if group(1) = out' ---> entered")
                    dict_text_out[m.group(2)] = m.group(2)
                    dict_params[m.group(2)] = "@"+m.group(2)

            param_box.add(table, False, False)
            param_ok = mforms.newButton()
            param_ok.set_text("OK")
            param_cancel = mforms.newButton()
            param_cancel.set_text("Cancel")

            mforms.Utilities.add_end_ok_cancel_buttons(param_box, param_ok, param_cancel)
            param_form.set_content(param_box)
            param_form.center()
            try:
                if param_form.run_modal(param_ok, param_cancel):
                    self.execute_sp(dict_params, dict_text_in, dict_text_out)
                self._list_parameters = []
            except:
                self._list_parameters = []
                raise
        else:
            self.__verbose_print_output("DEBUG FORM_PARAM 'ELSE IF APPEND_PARAMS' -- > do not entered in 'if append parameters to list'")
            self.execute_sp(False, False, False)
            
    """ for index, textentry in dict_textentry.items():
        self.print_output("text ALL PARAMS from key (" + index + ") value -> " + textentry.get_string_value()) """

    def __append_parameters_to_list(self):
        specific_name = 'procedure'
        script_parameters = """SELECT parameter_mode, parameter_name, data_type FROM information_schema.parameters
            WHERE routine_type = '{0}'
            AND specific_schema = '{1}'
            AND specific_name = '{2}';""".format(
                specific_name,
                self.schema_name,
                self.stored_procedure_name
            )
        try:
            result_parameters = self.debug_execute_script(script_parameters)
            while result_parameters and result_parameters.nextRow():
                params = ''.join([result_parameters.stringByName('parameter_mode'),
                    ";;",result_parameters.stringByName('parameter_name'),
                    ";;",result_parameters.stringByName('data_type')])
                self._list_parameters.append(params)

            if not self._list_parameters or b_any("in;;" in st for st in self._list_parameters):
                return False
            else:
                return True

        except:
            mforms.Utilities.show_message("Error", "Error on append_parameters_to_list() module", "OK", "", "")
            raise

    """ RDEBUG FUNCTIONS """

    def rdebug_start(self):
        session_id = self.get_worker_session()
        script="CALL common_schema.rdebug_start({0});".format(session_id)
        try:
            result = self.debug_execute_script(script)
            if result:
                self.__verbose_print_output("Debug has started!")
        except:
            mforms.Utilities.show_warning("Error!", "rdebug failed to start at {0} connection id!".format(session_id), "", "CANCEL", "")
            raise

    def rdebug_stop(self):
        script="CALL common_schema.rdebug_stop();"
        try:
            result = self.debug_execute_script(script)
            if result:
                self.__verbose_print_output("Debug has stopped!")
        except:
            mforms.Utilities.show_warning("Error!", "rdebug failed to stop", "", "CANCEL", "")
            raise     

    def execute_sp(self, params, params_in, params_out):
        script = "call {0}.{1}(".format(self.schema_name, self.stored_procedure_name)
        script_params_out = ""
        _list_out_params = []
        if params_out:
            i = 1
            script_params_out = "SET "
            for index, textout in params_out.items():
                t = "@"+index
                _list_out_params.append(t)
                script_params_out += t + " = NULL"
                script_params_out +=';' if i == len(params_out) else ', '
                i +=1
            self.print_output("script_params_out -> " + script_params_out)
            res = self.worker_execute_multiresult_script(script_params_out)

        if params:
            x = 1
            for k, param_name in params_in.items():
                script +="'" + param_name.get_string_value() + "'"
                script += ", " if x != len(params) else ''
                x +=1

            z = 1
            for k, param_name in params_out.items():
                script +="@" + param_name
                script += ", " if z != len(params_out) else ''
                z +=1

        script+=');'
        self.print_output("script --> " + script)

        self.rdebug_step_into()
        resultsets = self.worker_execute_multiresult_script(script)
        self.print_formatted_text(resultsets)

    def rdebug_run(self, event):
        self.rdebug_set_verbose(True)
        self.__verbose_print_output("rdebug_run(): worker session -> " + str(self.get_worker_session()))
        __list_breakpoints = self.rdebug_check_breakpoints()
        if not (__list_breakpoints) or not (__list_breakpoints.numRows() > 0):
            self.__verbose_print_output("No pre-breakpoints found!")
            self.rdebug_default_last_breakpoint()
            __list_breakpoints = self.rdebug_check_breakpoints()
            # self.__go_to_breakpoints(__list_breakpoints)
            while __list_breakpoints.nextRow():
                bp = __list_breakpoints.stringByName('statement_id')
                self.__add_remove_breakpoint(bp)
        
        self.__form_parameters()
        self.__verbose_print_output('putting event worker to wait')
        event.wait()

    """ Unused actually """
    # def __go_to_breakpoints(self, __list_breakpoints):
    #     while __list_breakpoints.nextRow():
    #             bp = __list_breakpoints.stringByName('statement_id')
    #             self.__add_remove_breakpoint(bp)

    def rdebug_set_verbose(self, boolean):
        script="CALL common_schema.rdebug_set_verbose("+str(boolean)+");"
        try:
            result = self.debug_execute_script(script)
            if result:
                self.__verbose_print_output("rdebug set verbose to {0}!".format(boolean))
        except:
            self.__verbose_print_output("rdebug failed to set verbose to {0}!".format(boolean))
            raise

    def rdebug_step_into(self, sst):
        self.__rdebug_run_debugger('into')

    def rdebug_step_out(self, sst):
        self.__rdebug_run_debugger('out')

    def rdebug_step_over(self, sst):
        self.__rdebug_run_debugger('over')

    def rdebug_default_last_breakpoint(self):
        script="select statement_id from common_schema._rdebug_routine_statements where routine_schema = '{0}' and routine_name = '{1}' order by statement_id desc limit 1;".format(
            self.schema_name,
            self.stored_procedure_name)
        try:
            result = self.debug_execute_script(script)
            if result and result.nextRow():
                script_add = "call common_schema.rdebug_set_breakpoint('{0}', '{1}', {2}, null, true);".format(
                    self.schema_name,
                    self.stored_procedure_name,
                    result.stringByName('statement_id'))
                try:
                    result2 = self.debug_execute_script(script_add)
                    if result2:
                        self.print_output("Default breakpoints added")
                except:
                    mforms.Utilities.show_message("Error", "Error on default_last_breakpoint()", "OK", "", "")
                    raise
        except:
            mforms.Utilities.show_message("Error", "Error on default_last_breakpoint()", "OK", "", "")
            raise

    def rdebug_check_breakpoints(self):
        script = "SELECT statement_id FROM common_schema._rdebug_breakpoint_hints WHERE worker_id = '{0}' and routine_schema = '{1}' and routine_name = '{2}'".format(
            self.get_worker_session(),
            self.schema_name,
            self.stored_procedure_name
        )
        try:
            result = self.debug_execute_script(script)
            if result:
                return result
        except:
            self.print_output("Failed to check breakpoints!")
            raise
    
    """ CONNECTION SESSIONS """
    
    def get_worker_session(self):
        return self.session_worker_id

    def get_debugger_session(self):
        return self.session_debugger_id

    def add_compiled_debug(self):
        self.worker_connector()
        self.debugger_connector()
        self.compile_debug(True)
        self.rdebug_start()

    def remove_compiled_debug(self):
        self.compile_debug(False)
        self.rdebug_stop()
        self.debug_connection.disconnect()

    def worker_connection(self):
        try:
            script = "SELECT CONNECTION_ID()"
            result = self.worker_execute_script(script)
            if result and result.nextRow():
                    self.__verbose_print_output("Successfully called a Worker");
                    self.session_worker_id = result.stringByName('CONNECTION_ID()')
                    self.__verbose_print_output("Worker ID: " + self.session_worker_id)
        except:
            raise

    def debugger_connection(self):
        try:
            script = "SELECT CONNECTION_ID()"
            result = self.debug_execute_script(script)
            if result and result.nextRow():
                self.__verbose_print_output("Successfully raised a Debugger")
                self.session_debugger_id = result.stringByName('CONNECTION_ID()')
                self.__verbose_print_output("Debugger ID: " + self.session_debugger_id)
        except:
            raise

    def worker_connector(self):
        """ Creates a new connection to 'worker' session starts """
        try:
            info = grt.root.wb.rdbmsMgmt.storedConns[0]
            self.work_connection = MySQLConnection(info)
            self.work_connection.connect()
            if self.work_connection.is_connected:
                self.worker_connection()
            else:
                mforms.Utilities.show_warning("Connection failed!", "Worker session failed to connect!", "OK", "", "")
        except:
            raise

    def debugger_connector(self):
        """ Creates a new connection to 'debugger' session starts """
        try:
            info = grt.root.wb.rdbmsMgmt.storedConns[0]
            self.debug_connection = MySQLConnection(info)
            self.debug_connection.connect()
            if self.debug_connection.is_connected:
                # mforms.Utilities.show_message("Connection Successfull!", "Debugger session is connected!", "OK", "", "")
                self.debugger_connection()
            else:
                mforms.Utilities.show_warning("Connection Failed!", "Debugger session failed to connect!", "OK", "", "")
        except:
            raise

    def compile_debug(self, baction):
        sch_name = self.schema_name
        sp_name = self.stored_procedure_name
        #params (worker_schema, worker_stored_procedure, boolean true/false add/remove debug code)
        params = "'"+sch_name+"', '"+sp_name+"', "+str(baction)
        script = "CALL common_schema.rdebug_compile_routine("+params+");"
        try:
            result = self.debug_execute_script(script)
            if not result:
                mforms.Utilities.show_message("Error!", "Not connected! \nCould not compile routine with debug", "OK", "", "")
            # else:
            #     mforms.Utilities.show_message("Success!", "added/removed compiled routine! " + str(baction), "OK", "", "") 
            #debug purpose 
            # if self.debug_connection.try_ping:
            #     mforms.Utilities.show_message("Success!", "ping and connection still alive! ", "OK", "", "") 
        except:
            raise
    
    def debug_execute_script(self, script):
        try:
            self.__verbose_print_output('Executing simple script via ('+threading.currentThread().getName()+') ...')
            result = self.debug_connection.executeQuery(script)
            if result:
                return result
        except:
            raise

    def debug_execute_multiresult_script(self, script):
        try:
            """ result = [list of MySQLResult] """
            self.__verbose_print_output('Executing multi result script via ('+threading.currentThread().getName()+') ...')
            result = self.debug_connection.executeQueryMultiResult(script)
            if result:
                return result
        except:
            raise

    def worker_execute_script(self, script):
        try:
            self.__verbose_print_output('Executing simple script via ('+threading.currentThread().getName()+') ...')
            result = self.work_connection.executeQuery(script)
            if result:
                return result
        except:
            raise

    def worker_execute_multiresult_script(self, script):
        try:
            """ result = [list of MySQLResult] """
            self.__verbose_print_output('Executing multi result script via ('+threading.currentThread().getName()+') ...')
            result = self.work_connection.executeQueryMultiResult(script)
            if result:
                return result
        except:
            raise

    def __rdebug_run_debugger(self, step_type):   
        if self.__dthread_running:
            self.print_output('Debugger is still running..')
        else:
            self.debug_running = True
            self.__dthread_running = True
            self.__debugger_thread(step_type)
            self.__worker_thread()
    
    def __debugger_thread(self, step):
        self.__dthread_event = threading.Event()
        __thread_debugger = threading.Thread(
            name='thread-step-{0}'.format(step),
            target=self.__rdebug_set_step,
            args = (step,
                    self.__dthread_event))
        self.__verbose_print_output('step_type: {0}'.format(step))
        self.__verbose_print_output('Starting thread... ' + __thread_debugger.getName())
        __thread_debugger.start()
        self.__update_timer = mforms.Utilities.add_timeout(0.2, self.__update_ui)
        self.__verbose_print_output('New thread ('+__thread_debugger.getName()+') is alive? ' + str(__thread_debugger.isAlive()))
    
    def __worker_thread(self):
        self.__wthread_event = threading.Event()
        __thread_worker = threading.Thread(
            name='thread-worker',
            target=self.rdebug_run,
            args = (self.__wthread_event,))

        self.__verbose_print_output('Starting thread... ' + __thread_worker.getName())
        __thread_worker.start()
        self.__verbose_print_output('New thread({0}) is alive? {1}'.
            format(__thread_worker.getName(),__thread_worker.isAlive()))
        
    def __rdebug_set_step(self, step, event):
        script = 'CALL common_schema.rdebug_step_{0}();'.format(step)
        try:
            event.wait(0.5)
            self.__verbose_print_output(threading.currentThread().getName() + ')) test?')
            for i in range(3):
                self.__verbose_print_output('testing please not freeze')
            event.set()
            self.__dthread_running = False
        except Empty:
            pass

    """ FIX ME """
    def __check_worker_status(self):
        """ if mysql Thread stop then set() worker """ 
        # script = "SELECT id, user, db, command, state, info FROM information_schema.processlist WHERE id = {0} and state = 'User sleep';".
        #     format(self.get_worker_session())
        