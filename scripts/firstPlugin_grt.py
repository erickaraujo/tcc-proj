# MySQL Workbench Plugin
# <1st Prototype for TCC Debug tool>
# Written in MySQL Workbench 6.3.9

from wb import *
import grt
#import mforms

#define this Python module as a GRT module
ModuleInfo = DefineModule(name="firstPlugin", author="Erick Araujo", version="1.0", description="Contains 1st Plugin Prototype")

#just a function used by plugin, its not exported
def printTableLine(fields, filler=" "):
    print "|"
    
    for text, size in fields:
        print text.ljust(size, filler), "|"

# This plugin takes no arguments (??)
#@ModuleInfo.plugin -> import to plugins (accessible in varios locations, such as menu and dropdowns)
#@ModuleInfo.plugin( "name of plugin", "caption to shown in places like menu", "description", "where to take arguments from", "where it'll be localized"
@ModuleInfo.plugin("wb.catalog.util.firstPlugin", caption="First Plugin Test", description="description", input=[wbinputs.currentCatalog()], pluginMenu="Catalog")

#ModuleInfo.export -> makes be exported by the module and also describes the return and argument types of the function
@ModuleInfo.export(grt.INT, grt.classes.db_Catalog)
def firstPlugin(catalog):
    # Put plugin contents here
    lines=[]
    schemalen=0
    columnlen=0
    typelen=0

    for schema in catalog.schemata:
        schema.len = max(schemalen, len(schema.name))

    for table in schema.tables:
        tablelen=max(tablelen, len(column.name))
    
    for column in table.columns:
        columnlen = max(columnlen, len(column.name))
        typelen=max(typelen, len(column.formattedType))
        lines.append((schema.name, table.name, column.name, column.formattedTypes(1)))

    for s,t,c,dt in lines:
        printTableLine([(s, schemalen), (t,tablelen), (c, columnlen), (dt, typelen)])

        printTableLine(
            [("-",schemalen),
             ("-",tablelen),
             ("-",columnlen),
             ("-",typelen)], "-")
        print len(lines), "columns printed"

    return 0

