SELECT CONNECTION_ID();


call common_schema.rdebug_start(327);
call common_schema.rdebug_compile_routine('sakila', 'film_in_stock', False);

call common_schema.rdebug_step_into();
call common_schema.rdebug_set_verbose(True);

call common_schema.rdebug_stop();


call common_schema.rdebug_show_routine_statements('sakila', 'film_in_stock');
call common_schema.rdebug_set_breakpoint('sakila', 'film_in_stock', 52, null, True);

call common_schema.rdebug_run();
call common_schema.rdebug_step_over();

call common_schema.rdebug_