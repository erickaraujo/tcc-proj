DELIMITER $$

DROP PROCEDURE IF EXISTS usp_fatorial_prima$$
CREATE PROCEDURE usp_fatorial_prima(IN number INT) comment 'APLICA DIVISÃO FATORIAL SOMENTE COM NUMEROS PRIMOS'
BEGIN
		DECLARE factor, divisor INT;
        
        SET factor = number;
        SET divisor = 2;
        
        myloop: WHILE (factor >= 1) DO
					IF MOD(factor, divisor) = 0 THEN
						BEGIN
							SET factor = divisor / divisor;
							SELECT divisor as 'divisor', factor as 'result';
						END;
					END IF;
					SET divisor =+ 1;
					IF factor = 1 THEN
						LEAVE myloop;
					END IF;
				END WHILE;
            
        
END$$

DELIMITER ;