import threading
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s',)


class SupportTest():

    def __init__(self):

        configs = {}
        configs['first_run'] = True
        configs['debug_status'] = 'stop'
        configs['counter'] = 1

        cs1 = threading.Thread()
        cs2 = threading.Thread()
        cs3 = threading.Thread()
        condition = threading.Condition()
        logging.debug('pré-worker is alive? ' + str(cs1.is_alive()))

        cs1 = threading.Thread(
            name='worker', target=self.getWorker, args=(condition, configs))
        cs2 = threading.Thread(
            name='debugger', target=self.getDebugger, args=(condition, configs))

        logging.debug('Starting program ' +
                      str(threading.current_thread().getName()))
        cs2.start()
        time.sleep(0.2)
        cs1.start()
        time.sleep(0.2)

        logging.debug('debugger is alive? ' + str(cs1.is_alive()))
        logging.debug('')
        time.sleep(0.2)
        logging.debug('who is alive?')
        self.whoIsAlive('cs1', cs1)
        self.whoIsAlive('cs2', cs2)
        self.whoIsAlive('cs3', cs3)
        logging.debug('')

        while configs['counter'] <= 4:
            logging.debug("Calling a new debugger..")
            cs3 = threading.Thread(
                name='debugger', target=self.getDebugger, args=(condition, configs))
            cs3.start()
            time.sleep(0.2)
            logging.debug('who is alive?')
            self.whoIsAlive('cs1', cs1)
            self.whoIsAlive('cs2', cs2)
            self.whoIsAlive('cs3', cs3)

    def getWorker(self, cv, configs):
        with cv:
            while configs['debug_status'] == 'run':
                logging.debug('running !!...')
                cv.wait()
            logging.debug('RECEIVED NOTIFY...')
            if configs['debug_status'] == 'stop':
                logging.debug('stopped !!...')

    def getDebugger(self, cv, configs):
        with cv:
            if configs['debug_status'] == 'run':
                logging.debug('debug is running!')
            elif configs['debug_status'] == 'stop':
                logging.debug('debug is not running!')
                configs['debug_status'] = 'run'
                logging.debug('debug started!')

            if configs['first_run']:
                logging.debug('First run!')
            else:
                logging.debug('Its not first run...')

            logging.debug('getDebuggerDone')
            configs['first_run'] = False

            if configs['counter'] == 4:
                logging.debug('now stopping')
                configs['debug_status'] = 'stop'
                cv.notify()
            else:
                logging.debug('nope')

            configs['counter'] += 1

    def whoIsAlive(self, name, thread):
        logging.debug('{0} -> {1}'.format(str(name), str(thread.is_alive())))
        time.sleep(0.2)


if __name__ == '__main__':
    st = SupportTest()
